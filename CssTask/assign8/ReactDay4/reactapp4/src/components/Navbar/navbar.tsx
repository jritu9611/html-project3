import react from "react"
import {NavbarHeader,Header,Button} from 'styles/components/index'
import headerLogo from 'assests/headerLogo.png'

export const Navbar=()=>{
    return (
       <NavbarHeader>
       <div></div>
       <Header>
        <img src={headerLogo} alt="logi"></img>
       </Header>
       <Button>Contact</Button>
       </NavbarHeader>
      
    )
}