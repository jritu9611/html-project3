import React from 'react';

import './App.css';
import {Navbar} from 'components/Navbar/navbar'
import {Sectionone} from 'components/Sectionone/main'

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Sectionone/>

    </div>
  );
}

export default App;
