import React, { useState } from 'react';

export function SimpleForm() {
 const [firstName, setFirstName] = useState('')
 const [lastName, setLastName] = useState('');
//   const [email, setEmail] = useState('');

  const handleSubmit = (e:any) => {
  
    console.log('Form submitted:',e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="firstName">First Name:</label>
      <input
        type="text"
        id="firstName"
        value="name"
        
  
      />

      <label htmlFor="lastName">Last Name:</label>
      <input
        type="text"
        id="lastName"
        
      />

      <label htmlFor="email">Email:</label>
      <input
        type="email"
        id="email"
       
      />

      <button type="submit">Submit</button>
    </form>
  );
}
