

import React from 'react'
import {NavLink,Outlet} from 'react-router-dom'
import { Landingheader,Headerpart1,Headerpart2,Headerpart3,Headeritem } from '../styles/Landingheader'



function Landingpage() {
   const navLink=(isActive:any)=>{
    return{
    color:isActive?"yello":"pink",
    }
  }
  return (
    <div>
        <Landingheader>
        <Headerpart1>
         {/* <Headeritem><NavLink to="/home">Outlet</NavLink></Headeritem>
         <Headeritem><NavLink to="/">Collection</NavLink></Headeritem>
         <Headeritem><NavLink to="/">Product</NavLink></Headeritem>
         <Headeritem><NavLink to="/">Home</NavLink></Headeritem> */}
        </Headerpart1>
        <Headerpart2>
        <Headeritem>Logo</Headeritem>
        <Headeritem>WIL</Headeritem>
        </Headerpart2>
        <Headerpart3>
            <Headeritem><NavLink to="/employee">Employee</NavLink></Headeritem>
            <Headeritem><NavLink to="/admin">Admin</NavLink></Headeritem>
            <Headeritem><NavLink to="/contact">Contact</NavLink></Headeritem>
            {/* <Headeritem><NavLink to="/sale">Sale</NavLink></Headeritem> */}
            
            

        </Headerpart3>
        </Landingheader>
    </div>
  )
}

export default Landingpage