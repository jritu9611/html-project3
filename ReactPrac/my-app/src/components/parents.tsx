import React from 'react'
import Child from './child'
import { Button } from 'semantic-ui-react';
import { useState } from 'react';
function Parents() {
    const [data, setData] = useState('');
  
  const parentToChild = () => {
    setData("This is data from Parent Component to the Child Component.");
  }
  return (
    <>
    <div><Child parentToChild={data} /></div>
    <div>
    <Button primary onClick={() => parentToChild()}>Click Parent</Button>
  </div>
  </>
  )
}

export default Parents