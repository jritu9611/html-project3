import styled from "styled-components"



export const Mainsalediv=styled.div`
height:100vh;
width:100%;
background-color:#212121;

`


export const Header=styled.div`

padding:100px;
display:flex;
justify-content:center;
color:#ffff;
font-size:40px;
font-style:italic;
font-variant:small-caps;
font-weight:bolder;
`


export const Conatiner=styled.div`
display:flex;
justify-content:center;

width:100%;
flex-wrap:wrap;

gap:50px;

`


export const Box=styled.div`
width:500px;
height:200px;

border:none;
box-shadow:2px 2px 2px 1px #212121;
&:hover{
    transition:1s;
    scale:1.1;
    
    
}
`

export const SaleButton=styled.div`
height:50px;
width:50px;
background-color:white;
color:#212121;
`