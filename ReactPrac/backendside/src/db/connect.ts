import mongoose from "mongoose"
const url="mongodb://localhost:27017/billingSystem"

export const dbConnection=()=>{
mongoose.connect(url)
.then(()=>{
    console.log("Database connected")
})
.catch((error)=>{
    console.log("Error")
})
}