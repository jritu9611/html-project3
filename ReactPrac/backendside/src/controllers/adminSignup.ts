 import Admin from "../models/Adminsign.js";
 import bcrypt from "bcrypt";
 import jwt from 'jsonwebtoken';

const SECRETKEY = "ADMINAPI";

export const signup = async(req:any,res:any) => {
 
      const {name,email,password}=req.body;
      try{
      const userExit=await Admin.findOne({email:email})
      if(userExit){
          return res.status(400).json({message:"User already exits"})
      }
      
              const hashpassword=await bcrypt.hash(password,10)
              const createUser=await Admin.create({
              name:name,
              email:email,
              password:hashpassword
          })
         const token=jwt.sign({email:createUser.email,id:createUser.id},SECRETKEY)
          res.status(201).json({user:createUser,token:token})
      }
  
  catch(error:any){
      console.log(error);
      res.status(500).json({ message: "Something went wrong" });
  }
};
