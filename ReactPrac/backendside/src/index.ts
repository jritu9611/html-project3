import express from "express"
import { dbConnection } from "./db/connect";
 import router  from "./routes/sigup";
 import bodyParser from 'body-parser';

const app=express();
const port=5000;
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))
 app.use("/api",router)
app.listen(port,()=>{
    console.log(`server running on port ${port}`)
    dbConnection()
})
app.get('/',(req,res)=>{
    res.send("welcome")
})