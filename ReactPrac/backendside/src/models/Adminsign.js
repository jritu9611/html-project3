import mongoose from "mongoose";


const adminModel=new mongoose.Schema(
    {
        name:{
            type:String,
            require:true,

        },
        email:{
            type:String,
            require:true,
        },
        password:{
            type:String,
            require:true,
        }
    },
    {timeseries:true}

)

const Admin=mongoose.model("Admin",adminModel)
export default Admin