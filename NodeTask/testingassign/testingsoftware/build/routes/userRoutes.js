"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userControllers_1 = require("../controllers/userControllers");
const posterControllers_1 = require("../controllers/posterControllers");
const auth_1 = __importDefault(require("../utils/auth"));
// const authenticateToken = require("../utils/auth");
const router = express_1.default.Router();
router.post("/signup", userControllers_1.signup);
router.post("/signin", userControllers_1.signin);
router.post("/emailsend", userControllers_1.emailsend);
router.patch("/changePassword", userControllers_1.changePassword);
router.post("/createPoster", auth_1.default, posterControllers_1.createPoster);
// router.post("/getPoster", getPoster);
exports.default = router;
