"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
// require("dotenv").config();
// here we are making application object and calling express fns
const app = (0, express_1.default)();
const userRoutes_1 = __importDefault(require("./routes/userRoutes"));
app.use((0, cors_1.default)());
app.use(body_parser_1.default.json());
app.use((0, express_fileupload_1.default)({
    useTempFiles: true
}));
app.use("/users", userRoutes_1.default);
//to convert body req(controller) to json
// app.use(express.json());
// we define api
app.get("/", (req, res) => {
    res.send("hellochi ritu is good");
});
const port = process.env.PORT;
mongoose_1.default
    .connect("mongodb://0.0.0.0:27017/Ritu")
    .then(() => {
    app.listen(5000, () => {
        console.log(`Server start on port 5000`);
    });
})
    .catch((error) => {
    console.log(error);
});
