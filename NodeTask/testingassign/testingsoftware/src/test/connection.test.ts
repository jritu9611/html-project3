import { faker } from '@faker-js/faker';
import mongoose from "mongoose";
import { describe, before, after } from "mocha";

import chai from "chai";
import chaiHttp from "chai-http";
import app from '../index'
chai.use(chaiHttp)
chai.should();
const userdata:any={
    email:faker.internet.email(),
    username:faker.name.fullName(),
    phone:faker.phone.number(),
    password:faker.internet.password(),
    }
    describe('signup', function () {
        it('signup user',(done)=>{
          
          chai
          .request(app)
          .post("/users/signup")
          .send(userdata)
          .end((err, res) => {
            // console.log(err)
            chai.expect(res.body).to.be.an("object");
           
          //res.body.data.should.have.property('username');
            //chai.expect(res.body).to.have.property("email");
            // chai.expect(res).to.have.property('password');
           
            done();
        })
      })
      afterEach(async () => {
          // Get a reference to the collection you want to clear
          const collection = mongoose.connection.collection('users');
          
          // Use the collection's `deleteMany` method to clear its contents
          await collection.deleteOne(userdata);
          
          // Disconnect from the database
          await mongoose.disconnect();
        });
      })
// describe('Register User', function () {
//   it('signup user',(done)=>{
   
//     chai
//     .request(app)
//     .post("/users/signup")
//     .send(userdata)
//     .end((err, res) => {
//       // console.log(err)
//       chai.expect(res.body).to.be.an("object");
//       console.log(res.body.user)
//       res.body.user.should.have.property('username');
//       //chai.expect(res.body).to.have.property("email");
//       // chai.expect(res).to.have.property('password');
//       done();
//   })
//   afterEach(async () => {
//     // Get a reference to the collection you want to clear
//     const collection = mongoose.connection.collection('users');
    
//     // Use the collection's `deleteMany` method to clear its contents
//     await collection.deleteOne(userdata);
    
//     // Disconnect from the database
//     await mongoose.disconnect();
// })
//   })
// })