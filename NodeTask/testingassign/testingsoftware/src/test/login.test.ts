import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import { describe, after, before } from "mocha";
import app from "../index";
import mongoose from "mongoose";
import { faker } from "@faker-js/faker";
chai.use(chaiHttp);

describe("login user", () => {
  before("connect and signup", (done) => {
    mongoose.createConnection("/mongodb://127.0.0.1:27017/Ritu");
    mongoose.connection.on("connected", (err, res) => {
      console.log("connection established");
    });
    done();
  });
  it("login user", async (done) => {
    const user: any = {
      email: faker.internet.email(),
      userId: faker.internet.userName(),
      password: faker.internet.password(),
      phone: faker.phone.number(),
    };
    chai
      .request(app)
      .post("/signup")
      .send(user)
      .then(() => {
        chai
          .request(app)
          .post("/signin")
          .send(user)
          .end((err, res) => {
            console.log("here we are testing login");
            expect(res).to.have.status(200);
            console.log(res.status);
          });
      });

    done();
  });
});
