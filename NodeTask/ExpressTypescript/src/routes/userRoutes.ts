import express, { Router } from "express";

import {
  signin,
  signup,
  emailsend,
  changePassword,
} from "../controllers/userControllers";
import { createPoster } from "../controllers/posterControllers";
import authenticateToken from "../utils/auth";
// const authenticateToken = require("../utils/auth");

const router = express.Router();
router.post("/signup", signup);
router.post("/signin", signin);
router.post("/emailsend", emailsend);
router.patch("/changePassword", changePassword);
router.post("/createPoster", authenticateToken,createPoster);
// router.post("/getPoster", getPoster);

export default router;
