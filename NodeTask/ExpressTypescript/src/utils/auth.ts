import express from "express";
import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

const SECRET_KEY = process.env.SECRET_KEY || "USERAPI";

const authenticateToken = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers["authorization"];
  const token = (authHeader && authHeader.split(" ")[1]) || "";
  if (token == null) {
      return res.sendStatus(401).json({ message: "please enter token" });
    }
    
    jwt.verify(token, SECRET_KEY, (err: any, decoded: any) => {
      console.log("token :", token, SECRET_KEY)
    if (err) {
      return res.status(401).json({ message: "token not provided" });
    } else {
      
      next();
    }
  });
};

export default authenticateToken;
