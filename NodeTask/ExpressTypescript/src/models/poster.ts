import mongoose,{ Schema, Document, Model } from "mongoose"



// here we will define object userSchema which tell us which property we have  to store inside schema

//scema help us to define model
const PosterSchema=new mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    discription:{
        type:String,
        required:true
    },
    image:{
      type:String
    },
    // timestamps:true this add two fiels inside schema createStaticHandler,modified 
},{timestamps:true})

module.exports=mongoose.model("Poster",PosterSchema);