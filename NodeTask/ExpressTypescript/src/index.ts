import  express from "express"
import { Request, Response, Router } from "express";

import mongoose from "mongoose"
import cors from "cors"
import bodyParser from "body-parser"
import  fileUpload from "express-fileupload"
import env from "dotenv"
env.config()
// require("dotenv").config();

// here we are making application object and calling express fns
const app = express();

import userRoutes from "./routes/userRoutes";


app.use(cors());

app.use(bodyParser.json());
app.use(fileUpload({
  useTempFiles:true
}))
app.use("/users", userRoutes);

//to convert body req(controller) to json
// app.use(express.json());

// we define api

app.get("/", (req: Request, res: Response) => {
  res.send("hellochi ritu is good");
});

const port = process.env.PORT;

mongoose
  .connect("mongodb://0.0.0.0:27017/Ritu")
  .then(() => {
    app.listen(5000, () => {
      console.log(`Server start on port 5000`);
    });
  })
  .catch((error: string) => {
    console.log(error);
  });
