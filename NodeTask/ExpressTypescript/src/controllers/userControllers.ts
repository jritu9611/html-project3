import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Request, Response } from "express";
import userModel from "../models/user";
import otpSchema from "../models/otp";

const nodeMailer = require("nodemailer");
const SECRET_KEY = "USERAPI";

export const signup = async (req: Request, res: Response) => {
  const { username, email, password, phone } = req.body;
  try {
    const existUser = await userModel.findOne({ email: email });
    if (existUser) {
      return res.status(400).json({ message: "User already exits" });
    }
    const hashPassword = await bcrypt.hash(password, 10);

    const result = await userModel.create({
      username: username,
      email: email,
      password: hashPassword,
      phone: phone,
    });


    
    const token = jwt.sign({ email: result.email, id: result.id }, SECRET_KEY);
    res.status(201).json({ user: result, token: token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};



export const signin = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const existUser = await userModel.findOne({ email: email });
  if (!existUser) {
    return res.status(400).json({ message: "user not found" });
  }
  const matchpass = await bcrypt.compare(password, existUser.password);
  if (!matchpass) {
    res.status(400).json({ message: "Password is wrong" });
  }
  const token = jwt.sign(
    { email: existUser.email, id: existUser.id },
    SECRET_KEY
  );
  res.cookie("jwtoken", token, {
    expires: new Date(Date.now() + 25992000000),
    httpOnly: true, //where we can add
  });

  res.status(201).json({ user: existUser, token: token });
};




export const emailsend = async (req: Request, res: Response) => {
  const { email, code, expireIn } = req.body;

  try {
    const exitEmail = await userModel.findOne({ email: email });
    if (exitEmail) {
      const otpCode = Math.floor(Math.random() * 10000 + 1);
      const otpData = new otpSchema({
        email: email,
        code: otpCode,
        expireIn: new Date().getTime() + 300 * 1000,
      });
      const otpResponse = await otpData.save();
      res
        .status(201)
        .json({ sucess: true, message: "Suceess Plaese Check your email" });
      mailer(email, otpCode);
    } else {
      res.status(200).json({ success: false, message: "Email does not exits" });
    }
  } catch (error: Error | any) {
    res.status(400).json({ success: false, msj: error.message });
  }
};




export const changePassword = async (req: Request, res: Response) => {
  // res.status(200).json({message:"ok"})
  const { email, otpCode, password } = req.body;
  const dataExit = await otpSchema.findOne({ email: email, code: otpCode });

  if (dataExit) {
    const currentTime = new Date().getTime();
    const diff = dataExit.expireIn - currentTime;
    if (diff < 0) {
      return res.status(300).json({ message: "Otp expire" });
    } else {
      let user = await userModel.findOneAndUpdate(
        { email: email },
        { password: password },
        { new: true }
      );
      const hashPassword = await bcrypt.hash(password, 10);
      //  user.password=password
      // user.save()
      res.status(200).json({ message: "password changed" });
    }
  } else {
    return res.status(400).json({ message: "Invalid otp" });
  }
};

const mailer = (email: any, otp: number) => {
  const nodemailer = require("nodemailer");
  let transporter = nodemailer.createTransport({
    service: "gmail",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "jritu961@gmail.com", // generated ethereal user
      pass: "efpjvexsvgnpjvyi", // generated ethereal password
    },
  });

  // send mail with defined transport object
  const mailOptions = {
    from: '"Fred Foo 👻" <jritu961@gmail.com>', // sender address
    to: "bar@example.com, baz@example.com", // list of receivers
    subject: "Hello ✔", // Subject line
    text: "Hello world?", // plain text body
    html: "<b>Hello world?</b>", // html body
  };

  transporter.sendMail(mailOptions, (error: any, info: any) => {
    if (error) {
      console.log(error);
    } else {
      console.log(`Email sent:` + info.response);
    }
  });
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account

  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
};
