"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPoster = exports.createPoster = void 0;
//all property of v2 will come in cloudinary variable
const cloudinary_1 = require("cloudinary");
// import multer from "multer"
const PosterSchema = require("../models/poster");
// Configuration 
cloudinary_1.v2.config({
    cloud_name: "djmulfeid",
    api_key: "259759478917581",
    api_secret: "PPPiOb4uVF2U2ZsoMI6CKDt9nQ0"
});
const createPoster = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    console.log(req.body);
    const file = (_a = req.files) === null || _a === void 0 ? void 0 : _a.photo;
    // console.log(file.tempFilePath)
    const response = yield cloudinary_1.v2.uploader.upload(file.tempFilePath);
    const ImgUrl = response.url;
    const { discription, image, title } = req.body;
    const posterData = yield PosterSchema.create({
        title: title,
        discription: discription,
        image: ImgUrl
    });
    res.status(201).json(posterData);
});
exports.createPoster = createPoster;
const getPoster = (req, res) => {
    const { email } = req.body;
};
exports.getPoster = getPoster;
