"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./Routes/routes"));
const app = (0, express_1.default)();
const connection = mongoose_1.default
    .connect("mongodb://localhost:27017/User")
    .then(() => {
    app.listen(3000, () => {
        console.log("server start");
        app.use(express_1.default.json());
        app.use("/", routes_1.default);
    });
})
    .catch((err) => console.log(err));
exports.default = connection;
