import mongoose from "mongoose";
import express from "express";
import UserRouter from "./Routes/routes";
const app = express();

const connection = mongoose
  .connect("mongodb://localhost:27017/User")
  .then(() => {
    app.listen(3000, () => {
      console.log("server start");

      app.use(express.json());
      app.use("/", UserRouter);
    });
  })
  .catch((err) => console.log(err));

export default connection;
