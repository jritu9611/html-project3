import { randomBytes } from "crypto";
import { Router } from "express";
import {UserController}  from "../controllers/index";
// import User from "../model/model";

const UserRouter = Router();

UserRouter.post("/addUser", UserController.UserAdd);

export default UserRouter;
