import mongoose from "mongoose";
import {IUser} from "../interfaces/IUser"

const userSchema = new mongoose.Schema<IUser>({
  name: {
    type: String,
    require: true,
  },
  age: {
    type: Number,
    require: true,
  },
  gender: {
    type: String,
    require: true,
  },
  courses: 
    [{ type: String,
      require: true,}],
  email: {
    type: String,
    require: true,
  },
  city: {
    type: String,
    require: true,
  },
});

const User = mongoose.model<IUser>("User", userSchema);

// Operations 
const getDocument = async ()=>{
  try{
    //const result = await User.find({age : "30"});
    //  ----- Logical Operators-----
    // OR Operator
    // const result = await User.find({$or :[{age : "25"},{city: "Ludhiana"}]});
    // console.log(result);
    // And Operator
    // const result = await User.find({$and:[{age : "30"},{city: "Lucknow"}]});
    // console.log(result);
    // Not Operator
    // const result = await User.find({"age": { $not: {$lt : 25}}});
    // console.log(result);
   // Nor Operator
    // const result = await User.find({$nor:[{age : "25"},{city: "Lucknow"}]});
    // console.log(result);


    // ---- Comparison Operator ------
    // const result = await User.find({"age": { $gt: 30}});
    // console.log(result);
    // const result = await User.find({"age": { $eq: 32}});
    // console.log(result);
    // const result = await User.find({"age": { $gte: 30}});
    // console.log(result);
    // const result = await User.find({"age": { $in: 25}});
    // console.log(result);
    // const result = await User.find({"age": { $lt: 25}});
    // console.log(result);
    // const result = await User.find({"age": { $lte: 32}});
    // console.log(result);
    // const result = await User.find({"age": { $ne: 22}});
    // console.log(result);
    // const result = await User.find({"age": { $nin: 30}});
    // console.log(result);

    // ---- Array operations----
    // const result = await User.find({ courses: { $all: [ "HTML", "CSS" ] } } );
    // console.log(result);
  //   const result = await User.find({
  //     age : {
  //         $elemMatch: {
  //             $lt: 30
  //         }
  //     }
  // }, );
  //   console.log(result);
  // const result = await User.find({ courses: { $size: 4 } }  );
  //   console.log(result);
  
  // ---- Projecter----
  // const result = await User.find({}, { courses: { $slice: 3 } } );
  //   console.log(result);
  // }
  
  // ---Update Operator----
//   const result = await User.updateOne({
//     age : 22
// }, {
//     $set: {
//         age: 24
//     }
// })
//     console.log(result);
  // const result = await User.find({age : "24"});
  // console.log(result);
  
  }
  catch(err){
    console.log(err);
  }
  
}

getDocument();
export default User;