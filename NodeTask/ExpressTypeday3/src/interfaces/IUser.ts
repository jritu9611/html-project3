export interface IUser {
    name: string;
    age: number;
    gender: "male" | "female";
    courses : Array<string>;
    email: string;
    city: string;
  }