const axios = require("axios");
const { parseString }= require('xml2js');
const xml = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
      <ubiNum>500</ubiNum>
    </NumberToWords>
  </soap:Body>
</soap:Envelope>
`;
const url = 'https://www.dataaccess.com/webservicesserver/NumberConversion.wso';

const fetchData = async()=>{
    const res = await axios.post(url,xml,{
    headers:{
        "Content-Type":"text/xml"
    }
    })
    let jsonData;
    //Parse the XML data to JSON
    parseString(res.data, (err, result) => {
    if (err) {
        console.log(err);
    } else {
       jsonData = result['soap:Envelope']['soap:Body'][0]['m:NumberToWordsResponse'][0]['m:NumberToWordsResult'];
    }
    console.log(jsonData);
})
}

fetchData();