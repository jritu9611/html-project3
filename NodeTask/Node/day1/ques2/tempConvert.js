const {default:axios} = require("axios");
const { parseString }= require('xml2js');

const xml = `<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <CelsiusToFahrenheit xmlns="https://www.w3schools.com/xml/">
      <Celsius>20</Celsius>
    </CelsiusToFahrenheit>
  </soap12:Body>
</soap12:Envelope>
`;
const url = 'https://www.w3schools.com/xml/tempconvert.asmx';

const fetchData= async()=>{
    const res = await axios.post(url,xml,{
        headers:{
            'Content-Type':' application/soap+xml; charset=utf-8'
        }
    })

    let jsondata;
    //Parse the XML data to JSON
    parseString(res.data, (err, result) => {
    if (err) {
        console.log(err);
    } else {
        jsondata=result['soap:Envelope']['soap:Body'][0]['CelsiusToFahrenheitResponse'][0]['CelsiusToFahrenheitResult']
    }
    console.log(jsondata);
})
}
fetchData();
