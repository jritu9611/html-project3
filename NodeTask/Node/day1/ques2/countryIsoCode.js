const axios = require("axios");
const { parseString }= require('xml2js');
const xml = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo">
      <sCountryISOCode>US</sCountryISOCode>
    </CapitalCity>
  </soap:Body>
</soap:Envelope>
`;
const url = 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso';

const fetchData = async()=>{
    const res = await axios.post(url,xml,{
    headers:{
        "Content-Type":"text/xml"
    }
    })
    let jsonData;
    //Parse the XML data to JSON
    parseString(res.data, (err, result) => {
    if (err) {
        console.log(err);
    } else {
        jsonData = result['soap:Envelope']['soap:Body'][0]['m:CapitalCityResponse'][0]['m:CapitalCityResult'];
    }
    console.log(jsonData);
})
}
fetchData();