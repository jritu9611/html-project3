const {default:axios} = require("axios");
const { parseString }= require('xml2js');

const xml = `<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <ListOfCountryNamesByName xmlns="http://www.oorsprong.org/websamples.countryinfo">
    </ListOfCountryNamesByName>
  </soap12:Body>
</soap12:Envelope>

`;
const url = 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso';

const fetchData= async()=>{
    const res = await axios.post(url,xml,{
        headers:{
            'Content-Type':'text/xml;charset = utf-8'
        }
    })
    
    //Parse the XML data to JSON
    parseString(res.data, (err, result) => {
    if (err) {
        console.log(err);
    } else {
        const jsondata = result['soap:Envelope']['soap:Body'][0]['m:ListOfCountryNamesByNameResponse'][0]['m:ListOfCountryNamesByNameResult'][0]['m:tCountryCodeAndName'];
        jsondata.map((item)=>{
                return(
                    console.log(item['m:sName'])

                )
        });
    }
})
}
fetchData();
