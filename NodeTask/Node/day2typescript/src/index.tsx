import express from 'express';
 
// Initialize the express engine
const app: express.Application = express();
 
// Take a port 3000 for running server.
const port: number = 3000;
 
// Handling '/' Request
app.get('/', (_req, _res) => {
    _res.send("TypeScript With Express");
});
 
// Server setup
app.listen(port, () => {
    console.log(`TypeScript with Express
         http://localhost:${port}/`);
});


































// const express = require("express");
// const mongoose = require("mongoose");

// const cors = require("cors");
// require("dotenv").config();
// const bodyParser = require("body-parser");
// // here we are making application object and calling express fns
// const app = express();

// const userRoutes = require("./routes/userRoutes");

// app.use(cors());
// app.use(bodyParser.json());
// app.use("/users", userRoutes);


// //to convert body req(controller) to json
// app.use(express.json());

// // we define api

// app.get("/", (req, res) => {
//   res.send("hellochi ritu is good");
// });

// const port = process.env.PORT;

// mongoose
//   .connect(
//     "mongodb+srv://jritu961:1234@cluster0.zojmwmo.mongodb.net/?retryWrites=true&w=majority"
//   )
//   .then(() => {
//     app.listen(port, () => {
//       console.log(`Server is start on port 5000`);
//     });
//   })
//   .catch((error) => {
//     console.log(error);
//   });
