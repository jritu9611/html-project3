const { default: mongoose } = require("mongoose");

const mongoose=require(mongoose)

// here we will define object userSchema which tell us which property we have  to store inside schema

//scema help us to define model
const PosterSchema=mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    body:{
        type:String,
        required:true
    },
    userId:{
        type:mongoose.Schema.types.ObjectId,
        ref:"User",
        required:true
    },
    // timestamps:true this add two fiels inside schema createStaticHandler,modified 
},{timestamps:true})

module.exports=mongoose.model("Poster",PosterSchema);