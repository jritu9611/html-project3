const mongoose = require("mongoose");

//here we will define object userSchema which tell us which property we have  to store inside schema

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    //whenever we update the  password it will create empty token fiels in database"
    resetToken: {
      type: String,
    },
    expiredToken: {
      type: String,
    },
    // timestamps:true this add two fiels inside schema createStaticHandler,modified
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
