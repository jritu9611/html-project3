const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const userModel = require("../models/user");
const randomString = require("randomstring");
const nodeMailer = require("nodemailer");
const SECRET_KEY = "USERAPI";

const signup = async (req, res) => {
  const { username, email, password } = req.body;
  try {
    const existUser = await userModel.findOne({ email: email });
    if (existUser) {
      return res.status(400).json({ message: "User already exits" });
    }
    const hashPassword = await bcrypt.hash(password, 10);

    const result = await userModel.create({
      username: username,
      email: email,
      password: hashPassword,
    });

    const token = jwt.sign({ email: result.email, id: result.id }, SECRET_KEY);
    res.status(201).json({ user: result, token: token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

const signin = async (req, res) => {
  const { email, password } = req.body;
  const existUser = await userModel.findOne({ email: email });
  if (!existUser) {
    return res.status(400).json({ message: "user not found" });
  }
  const matchpass = await bcrypt.compare(password, existUser.password);
  if (!matchpass) {
    res.status(400).json({ message: "Password is wrong" });
  }
  const token = jwt.sign(
    { email: existUser.email, id: existUser.id },
    SECRET_KEY
  );
  res.status(201).json({ user: existUser, token: token });
};

const forgetPasswod = async (req, res) => {
  const { email } = req.body;

  try {
    const exitEmail = await userModel.findOne({ email: email });
    if (exitEmail) {
    } else {
      res.send(200).json({ success: false, message: "Email does not exits" });
    }
  } catch (error) {
    res.send(400).json({ success: false, msj: error.message });
  }
};
module.exports = { signin, signup };
