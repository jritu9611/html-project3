const express = require("express");
const userRouter = express.Router();
const { signin, signup } = require("../controllers/userControllers");
userRouter.post("/signup", signup);

userRouter.get("/signin", signin);

module.exports = userRouter;
