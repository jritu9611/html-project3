
const express = require("express");
const app = express();
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Origin",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  next();
});
const io = require("socket.io")(8000);

const users = {};

io.on("connection", (socket) => {
   
  socket.on("new-user-joined", (nameUser) => {
    //console.log("New-user", nameUser);
    users[socket.id] = nameUser;
    socket.broadcast.emit("user-joined", nameUser);
  });

  socket.on("send", (message) => {
    socket.broadcast.emit("receive", {
      message: message,
      nameUser: users[socket.id],
    });
  });
  socket.on("disconnect", (message) => {
    socket.broadcast.emit("left", users[socket.id]);
    delete users[socket.id];
  });
});






















// const express = require("express");
// const app = express();
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Origin",
//     "Origin,X-Requested-With,Content-Type,Accept"
//   );
//   next();
// });


// const io=require('socket.io')(12000)

// const users={}

// io.on('connection',socket=>{
//     socket.on('new-user-join',userName=>{
//         console.log("new-user",userName)
//         users[socket.id]=userName;
//         socket.broadcast.emit('user-joined',userName)
//     });

//     socket.on('send',message=>{
//         socket.broadcast.emit('receive',{message:message,userName:users[socket.id]})
//     })
// })