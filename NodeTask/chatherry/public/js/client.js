const socket = io("http://192.168.10.73:8000", {
  transports: ["websocket", "polling", "flashsocket"],
});

const form = document.getElementById("send-container");
const messageInput = document.getElementById("messageInp");
const messageContainer = document.querySelector(".container");
let audio = new Audio("/tune.mp3");

const append = (message, position) => {
  const messageElement = document.createElement("div");
  messageElement.innerText = message;
  messageElement.classList.add("message");
  messageElement.classList.add(position);
  messageContainer.append(messageElement);
  
};

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const message = messageInput.value;
  append(`You : ${message}`, "right");
  socket.emit("send", message);
  messageInput.value = "";
});
const nameUser = prompt("Enter your name to join");
socket.emit("new-user-joined", nameUser);

socket.on("user-joined", (nameUser) => {
  append(`${nameUser} joined the chat`, "right");
});
socket.on("receive", (data) => {
  append(`${data.nameUser} : ${data.message}`, "left");
});
socket.on("left", (nameUser) => {
  append(`${nameUser} Left the chat`, "left");
});





























// const socket=io('http://localhost:12000',{transports:['websocket','polling','flashsocket']})

// const form=document.getElementById('send-container')
// const messageInput=document.getElementById('messageInp')
// const messageContainer=document.querySelector(".container")

// const append = (message, position) => {
//     const messageElement = document.createElement("div");
//     messageElement.innerText = message;
//     messageElement.classList.add("message");
//     messageElement.classList.add(position);
//     messageContainer.append(messageElement);
   
//   };
  

//   form.addEventListener("submit", (e) => {
//     e.preventDefault();
//     const message = messageInput.value;
//     append(`You : ${message}`, "right");
//     socket.emit("send", message);
//     messageInput.value = "";
//   });
  

// const userName=prompt("Enter Your Name")
// socket.emit('new-user-join',userName)

// socket.on("user-joined", (nameUser) => {
//   append(`${nameUser} joined the chat`, "right");
// });
// socket.on("receive", (data) => {
//   append(`${data.nameUser} : ${data.message}`, "left");
// });
// socket.on("left", (nameUser) => {
//   append(`${nameUser} Left the chat`, "left");
// });


