
var parseString=require('xml2js').parseString
const axios=require('axios')

const url='http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso'

const xmlData=`<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <ListOfCountryNamesByName xmlns="http://www.oorsprong.org/websamples.countryinfo">
    </ListOfCountryNamesByName>
  </soap12:Body>
</soap12:Envelope>
`

const fetchData=async ()=>{
    const response =await axios.post(url,xmlData,{
        headers:{'Content-Type':'text/xml'}
    })
    const data=response.data;
    parseString(data,(err,results)=>{
   
   console.log(results['soap:Envelope'])
    })
    
}
fetchData()