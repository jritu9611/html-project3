import { Routes, Route } from "react-router-dom"
import { RoutingPage } from "routes";

import React from 'react';
import { QueryClient, QueryClientProvider } from "react-query";
// import logo from './logo.svg';
// import './App.css';
// import {Login} from './components/Login/index'
// import {Dashboard} from './components/Dashboard'
// import {Modal} from './components/Modal'
// import {User} from './components/User'
// import {Services} from './services/index'
const queryClient=new QueryClient()
function App() {
  return (
    <QueryClientProvider client={queryClient}>
    <div className="App">
      <RoutingPage></RoutingPage>
      {

      }
    </div>
    </QueryClientProvider>
  );
}

export default App;
