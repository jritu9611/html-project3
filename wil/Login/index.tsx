import { useForm } from "react-hook-form";
import { useNavigate } from "react-router";
import * as yup from "yup"
import {yupResolver} from '@hookform/resolvers/yup'
import {
  Logincontainer,
  Loginpage,
  Witsimage,
  Header,
  Newuserpara,
  Createaccount,
  Input,
  Forgetdiv,
  Button
} from "styles/components/Login";
import witslogo from "assets/witslogo.png";
import { ILoginData } from "interfaces/UserLogin";


const LoginSchema = yup.object().shape({
  email: yup.string().email('Invalid email').required('Email is required'),
  password: yup.string().required('Password is required'),
});

export const Login = () => {
  const navigate=useNavigate();
  const { register,handleSubmit,formState:{errors}} = useForm<ILoginData>({resolver:yupResolver(LoginSchema)});
  const getData=(data:any)=>{
    if(data){
    localStorage.setItem("user",JSON.stringify(data))
  }
  navigate("/");
}
  // const onSubmit=(data:any)=>{
  // console.log(data)
  // }
  return (
    
    <Logincontainer>
      <Loginpage>
        <Witsimage src={witslogo} alt="Wits Logo"></Witsimage>
        <Header>Login With WIL</Header>
        <Newuserpara>
          New User? <Createaccount href="">Create an Account</Createaccount>
        </Newuserpara>
         <form onSubmit={handleSubmit(getData)}>
          <Input placeholder="Email Address" { ...register("email") } type="email"></Input>
          {errors.email && <span>{errors.email.message}</span>}
          <br></br>
          <Input placeholder="Password" { ...register("password") } type="password"></Input>
          {errors.password && <span>{errors.password.message}</span>}
          <Forgetdiv >Forget Password?</Forgetdiv>
          <Button>Login</Button>
          </form>
      </Loginpage>
    </Logincontainer>
  );
};
