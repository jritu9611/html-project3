import React from "react";
const FormData = () => {
  const register = (e) => {
    e.preventDefault();

    //  Method1
    const firstName = document.getElementById("firstName");
    console.log(firstName.value);
    const lastName = document.getElementById("lastName");
    console.log(lastName.value);

    const gender = document.getElementById("gender");
    console.log(gender.value);
    const age = document.getElementById("age");
    console.log(age.value);
    const contact = document.getElementById("contact");
    console.log(contact.value);
    const gmail = document.getElementById("gmail");
    console.log(gmail.value);
    const addess = document.getElementById("addess");
    console.log(addess.value);

    // Method2 but here we cant get gender due to select
    // const allInput = document.querySelectorAll("input");
    // for (let i = 0; i < allInput.length; i++) {
    //   if (allInput[i].value === "") {
    //     alert("Please Enter Data");
    //     return
    //   }

    //   console.log(allInput[i].value);
    // }
  };

  return (
    <div className="main">
      <div>
          <h1>Register User</h1>
        <form id="formData" onSubmit={register}>
          <div className="bothDiv">
          <div className="formContainer">
            <label>First Name</label>

            <input type="text" id="firstName"></input>

            <label>Last Name</label>

            <input type="text" id="lastName"></input>

            <label>Gender</label>

            <select id="gender">
              <option>Select</option>
              <option>Male</option>
              <option>Female</option>
            </select>

            <label>Age</label>

            <input type="text" id="age"></input>

            <label>Contact Number</label>

            <input type="text" id="contact"></input>

            <label>Gmail</label>

            <input type="text" id="gmail"></input>

            <label>Address</label>

            <input id="addess"></input>

            <button type="submit">Register</button>
          </div>
          <div>
            <img src="https://img.lovepik.com/element/45009/2341.png_300.png" />
          </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormData;
