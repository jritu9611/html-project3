import React, { useEffect ,useState} from "react";
import {Table,TableHeader,TableData,TableRow} from 'styled/containers/FormCss'
const RenderData=()=>{
    const [data, setData] = useState([]);

  const fetchData = () => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((actualData) => {
        console.log(actualData);
        setData(actualData);
        console.log(data);
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
  <div>
    
      <Table>
      <TableRow>
       
        <TableHeader>userId</TableHeader>
        <TableHeader>id</TableHeader>
        <TableHeader>title</TableHeader>
        <TableHeader>completed</TableHeader>
       
      </TableRow>
      {data.map((item: any) => (
        <TableRow>
          
          <TableData>{item.userId}</TableData>
          <TableData>{item.id}</TableData>
          <TableData>{item.title}</TableData>
          <TableData>{`${item.completed}`}</TableData>
          
        </TableRow>
      ))}
      </Table>
    
    </div>
  
  );
};
//     let API='https://jsonplaceholder.typicode.com/todos'
//     const fetchApiData=async(url:any)=>{
//    try{
//     const res=await fetch(url)
//     const data=await res.json();
//     console.log(data)
    
//     }
   
//    catch(error){
//    console.log("Error")
//    }
//     }
//     useEffect(()=>{
//         fetchApiData(API)
//     })
    
//     return(
//        <div>
//        </div>
//     )

export default RenderData