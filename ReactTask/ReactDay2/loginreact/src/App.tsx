// import React from 'react';
// import logo from './logo.svg';
import {BrowserRouter,Route,Routes} from "react-router-dom"
import Login from 'components/Login/login'
import RenderData from 'components/Login/render'

import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />}> </Route>
        <Route path="/data" element={<RenderData />}></Route>
      </Routes>
  </BrowserRouter>
   
  )
}

export default App;
