import { useQuery} from "react-query";
import dataFetch from "services/dataFetch";


 export const useFetch = (url: string) => {

  async function fetchData() {
   try{
    const response = await dataFetch.get(url);
    return response.data;
  }
  catch(error){
    console.log(error); 
  }
}
return useQuery("user",fetchData,{
  refetchOnWindowFocus:false
});
}








// import React from "react";
// import axios from "axios";
// import { IUser } from "interfaces";

// const useFetch = (url: string) => {
//   const [data, setData] = React.useState<IUser[] | null>(null);
//   const [loading, setLoading] = React.useState(true);

//   React.useEffect(() => {
//     const fetchData = async () => {
//       const result = await axios(url);
//       setData(result.data);
//       setLoading(false);
//     };
//     fetchData();
//   }, [url]);

//   return { data, loading };
// };

// export default useFetch;
