import { useFetch } from "hooks/useFetch";
import {
  TableContainer,
  TableMain,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from "styles/ProductTable/index";

export const UserTable = () => {
  const data = useFetch("/users");

  let fetchedData = data?.data;
  // console.log(data);

  return (
    <TableContainer>
      <TableMain>
        <Thead>
          <Tr>
            <Th>
              <input type={"checkbox"} />
            </Th>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Description</Th>
          </Tr>
        </Thead>
        <Tbody>
          {fetchedData?.slice(0, 2).map((item: any) => (
            <Tr key={item.id}>
              <Td>
                <input type={"checkbox"} />
              </Td>
              <Td>{`${item?.name?.firstname} ${item?.name?.lastname}`}</Td>
              <Td>{item.email}</Td>
              {/* <Td style={{textAlign:"left"}}>{item.description}</Td>  */}
              <Td>
                If you don’t yet use npm or a modern module bundler, and would
                rather prefer a single-file UMD build that makes ReactRedux
                available as a global object, you can grab a pre-built version
                from cdnjs.
              </Td>
              {/* <Td>{item.name}</Td>
               */}
            </Tr>
          ))}
        </Tbody>
      </TableMain>
    </TableContainer>
  );
};
