import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import poweroff from "assets/poweroff.png";
import witslogo from "assets/witslogo.png";
import circle from "assets/circle.png";
import square from "assets/square.png";
import {ProductTable} from "components/ProductTable"
// import { useHistory } from "react-router-dom";
import {
  Navbar,
  Imagewits,
  IconHeader,
  Icon,
  Data,
  Main,
  Productdiv,
  Header,
  Name,
  Add,
  PowerImage,
} from "styles/components/Dashboard";
import { Modal } from "components/Modal";
import { UserTable } from "components/UserTable";
const handleLogout = () =>{
  localStorage.clear();
  window.location.reload();
}


export const Dashboard = () => {
  const buttoncolor={
    backgroundColor: "DodgerBlue",
  }
  const navigate = useNavigate();
  const [showModal, setShowModal] = useState(false);
  const [userData, setuserData] = useState(false);
  const [active, setActive] = useState(false);
  const handleClick = () => {
    setActive(!active);
  };
  const handleButton=()=>{
    localStorage.clear();
    window.location.reload()
  }
  // const history = useHistory();
  
  // const productPage = () => {
  //     history.push("/courses")
  // }

  
  return (
    <Main>
      <Navbar>
        <Imagewits src={witslogo} alt="wits"></Imagewits>
        <IconHeader
          onClick={()=>{handleClick();
            // productPage()
          }}
          style={{ backgroundColor: active ? "#FFEBB1" : "#FAFAFA" }}
        >
          <Icon>
            <img src={square}></img>
          </Icon>
          <Data>Product</Data>
        </IconHeader>
        <IconHeader
          onClick={() => {
            handleClick();
          }}
          style={{ backgroundColor: active ? "#FAFAFA" : "#FFEBB1" }}
        >
          <Icon>
            <img src={circle}></img>
          </Icon>
          <Data>Users</Data>
        </IconHeader>
      </Navbar>
      <Productdiv>
        <PowerImage>
          <button onClick={handleButton} style={{border: "none"}} ><img src={poweroff} alt="power"></img></button>
        </PowerImage>
        <Header>
          <Name>Product</Name>
          {/* <Add onClick={()=>navigate("modal")}>Add Product</Add> */}
          <Add onClick={() => setShowModal(true)}>Add Product</Add>
          {showModal && <Modal/>}
        </Header>
        
        <ProductTable></ProductTable>
     
      </Productdiv>
      
    </Main>
    
  );
};
