import { useForm } from "react-hook-form";
import { useNavigate } from "react-router";

import {
  Logincontainer,
  Loginpage,
  Witsimage,
  Header,
  Newuserpara,
  Createaccount,
  Input,
  Forgetdiv,
  Button
} from "styles/components/Login";
import witslogo from "assets/witslogo.png";




export const Login = () => {
 
  const { register,handleSubmit,formState:{errors}} = useForm();
  
  // const onSubmit=(data:any)=>{
  // console.log(data)
  // }
  return (
    
    <Logincontainer>
      <Loginpage>
        <Witsimage src={witslogo} alt="Wits Logo"></Witsimage>
        <Header>Login With WIL</Header>
        <Newuserpara>
          New User? <Createaccount href="">Create an Account</Createaccount>
        </Newuserpara>
       
          <Input placeholder="Email Address" { ...register("email") } type="email"></Input>
         
          <br></br>
          <Input placeholder="Password" { ...register("password") } type="password"></Input>
         
          <Forgetdiv >Forget Password?</Forgetdiv>
          <Button>Login</Button>
          
      </Loginpage>
    </Logincontainer>
  );
};
