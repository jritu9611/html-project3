import { useFetch } from "hooks/useFetch";
import {
  TableMain,
  Thead,
  Tr,
  Th,
  Td,
  
  Tbody
} from "styles/ProductTable/index";

export const ProductTable = () => {
  const data = useFetch("/products");

  let fetchedData = data?.data;

  return (
   
      
      <TableMain>
        <Thead>
          <Tr>
            <Th><input type={"checkbox"} /></Th>
            <Th >Name</Th>
            <Th>Price</Th>
            <Th>Description</Th>
          </Tr>
        </Thead>
        <Tbody>
          {fetchedData?.slice(0,4).map((item: any) => (
            <Tr key={item.id}>
              <Th><input type={"checkbox"} /></Th>
              <Td>{item.title}</Td>
              <Td>{item.price}</Td>
              <Td style={{textAlign:"left"}}>{item.description}</Td>  
            </Tr>
          ))}
        </Tbody>
      </TableMain>
  );
};

// import axios from "axios";
// import { useState,useEffect } from "react";
// import React from "react";

// export  function Services() {
//     const [data,setData]=useState([])
//    useEffect(()=>{
//     axios.get("https://fakestoreapi.com/products")
//     .then((res)=>{console.log(res.data)

//         //  setData(res?.data?.dada)
//     })
//     .catch(err=>{console.log("Error")})
//   },[])
//   return (
//     <div>
//        <table>
//         <thead>
//           <tr>
//             <th>id</th>
//             <th>title</th>

//             <th>Price</th>

//           </tr>

//         </thead>
//         <tbody>

//      {data?.map((item:any,index:any)=>{
//         return(

//             <tr key={item.id}>
//             <td>{item.id}</td>
//             <td>{item.title}</td>
//             <td>{item.price}</td>
//           </tr>
//           )})}

// </tbody>
//       </table>
//     </div>
//   );
// }
