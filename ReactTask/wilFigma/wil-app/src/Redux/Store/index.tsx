import { combineReducers } from '@reduxjs/toolkit'
import {configureStore} from '@reduxjs/toolkit'
import ProductPage from "Redux/ProductPage";
import UserSlice from "Redux/UserSlice";

const reducers = combineReducers({page:ProductPage, product:UserSlice});
export const store = configureStore({
    reducer: reducers
});
