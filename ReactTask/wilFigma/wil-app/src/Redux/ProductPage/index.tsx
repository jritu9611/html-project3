import { createSlice,PayloadAction } from "@reduxjs/toolkit";

const initialState ="products"

const ProductPage = createSlice({
    name:"page",
    initialState:initialState,
    reducers:{
        changePage:(state:any, action:PayloadAction<any>)=>{
            state= action.payload;
            return state;
        }
    }
})
export const {changePage} = ProductPage.actions;
export default ProductPage.reducer;
