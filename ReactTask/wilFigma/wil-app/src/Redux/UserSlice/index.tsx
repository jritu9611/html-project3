import { createSlice } from "@reduxjs/toolkit";
import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";

const initialState: any[] = [];

const UserSlice = createSlice({
    name:"product",
    initialState :initialState,
    reducers:{
       combine:(state:any, action:PayloadAction<any>)=>{
        state=[...action.payload, ...state]
        return state;
       }
    }
})

export const {combine} = UserSlice.actions;
export default UserSlice.reducer
