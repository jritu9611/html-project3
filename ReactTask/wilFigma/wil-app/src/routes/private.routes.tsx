import { Navigate } from "react-router-dom";
import isLogin from "utils/isLogin";

const PrivateData = (props: any) => {
  const { component } = props;
  return !isLogin() ? <Navigate to="/login" /> : component;
};

export default PrivateData;