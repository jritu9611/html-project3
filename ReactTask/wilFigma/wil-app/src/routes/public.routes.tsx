import { Navigate } from "react-router-dom";
import isLogin from "utils/isLogin";

export const PublicData = (props: any) => {
  const { component } = props;
  return isLogin() ? <Navigate to="/" /> : component;
};