import { Routes, Route } from "react-router-dom";

import React from "react";
// import { IRoute } from "interfaces";
import { Login } from "components/Login/index";
import { Dashboard } from "components/Dashboard";
import { Modal } from "components/Modal";
import { UserTable } from "components/UserTable";
// import {Services} from 'services/index'
import PrivateData from "routes/private.routes";
import { PublicData } from "routes/public.routes";
import { ProductTable } from "components/ProductTable";

const myRoutes = [
  {
    component: <Dashboard />,
    path: "/",
    restricted: true,
  },
  {
    component: <Login />,
    path: "/login",
    restricted: false,
  },
  {
    component: <UserTable />,
    path: "user",
    restricted: false,
  },
  {
    component: <ProductTable />,
    path: "/product",
    restricted: false,
  },
  // {
  //   component: <Services/>,
  //   path: "services",
  //   restricted: false,
  // },
];
export const RoutingPage = () => {
  return (
    <Routes>
      {myRoutes.map((obj) => {
        return (
          <Route
            path={obj.path}
            element={
              obj.restricted ? (
                <PrivateData component={obj.component} />
              ) : (
                <PublicData component={obj.component} />
              )
            }
          />
        );
      })}
    </Routes>
  );
};
