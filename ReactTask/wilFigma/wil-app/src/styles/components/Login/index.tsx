import styled from "styled-components";


import loginBackground from "assets/loginBackground.png";

export const Logincontainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: url(${loginBackground});
  min-height: 100vh;
  min-width: 100%;
  background-repeat: no-repeat;
  background-size: 100% 105%;
`;

export const Loginpage = styled.div`
  min-height: 556px;
  min-width: 550px;
  background-color: #ffffff;
  
 padding-left:50px;
 
`;

export const Witsimage = styled.img`

 
    margin-top:60px;
    
  width: 195px;
  height: 85px;
`;

export const Header = styled.div`
  font-family: 'Inter';
font-style: normal;
font-weight: 800;
font-size: 30px;
line-height: 36px;

color: #272727;

`;

export const Newuserpara = styled.p`
font-family: 'Poppins';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;
/* identical to box height */


color: black;


`

export const Createaccount = styled.a`
color:#1D2E88; 
font-style: normal;
font-weight: 700;
font-size: 16px;
line-height: 24px;

`
export const Input=styled.input`
color:#000000;
min-width:470px;
min-height:50px;
background: #FFFFFF;
border-radius: 10px;
padding-left:25px;
font-family: 'Poppins';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;
margin-bottom:10px;
opacity: 0.2;
border: 1px solid #000000;

`
export const Forgetdiv=styled.div`
display:flex;
justify-content:end;
font-family: 'Poppins';
font-style: normal;
font-weight: 800;
font-size: 19px;
line-height: 35px;
/* identical to box height */
margin-right:50px;

color: #1D2E88;

`



export const Button=styled.button`
border-radius: 10px;
min-width:500px;
min-height:64px;
font-family: 'Poppins';
font-style: normal;
font-weight: 700;
font-size: 18px;
line-height: 27px;
/* identical to box height */
border: 0px solid #000000;
background: #1D2E88;
color: #FFFFFF;
`




