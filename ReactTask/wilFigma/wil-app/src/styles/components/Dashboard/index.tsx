import styled from "styled-components";

export const Navbar = styled.div`
  padding: 40px;
  width: 258px;
  height: 1000px;
  background: #ffebb1;
`;

export const Imagewits = styled.img`
  margin-bottom: 30px;
`;

export const IconHeader = styled.button`
  display: flex;
  padding: 10px;
  margin-top: 20px;
  border-radius: 18px 0px 0px 18px;
  background: #ffebb1;
  border: none;
  width: 298px;
  gap: 25px;
`;

export const Icon = styled.div``;

export const Data = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  /* identical to box height */
  color: #000000;
`;

export const Main = styled.div`
  display: flex;
  width: 100%;
`;

export const Productdiv = styled.div`
  width: 100%;
  margin-left: 30px;
`;
export const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;
export const Name = styled.div`
  width: 92px;
  height: 29px;
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 29px;
  /* identical to box height */

  color: #000000;
`;
export const Add = styled.button`
  width: 143px;
  height: 40px;
  background: #1d2e88;
  border-radius: 20px;
  font-family: "Inter";
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 17px;
  /* identical to box height */
  border: none;

  color: #ffffff;
`;

export const PowerImage = styled.div`
  margin: 20px;
  display: flex;
  justify-content: end;
  background: #fafafa;
`;
