import styled from "styled-components"

export const Container=styled.div`
display:flex;
justify-content:center;
width:100%;
height:100vh;
    
    
`

export const Wrapper=styled.div`

position:fixed;
left:0px;
right:0px;
bottom:0px;
top:0px;
background-color:rgba(189,189,189,0.9);
`


export const Box=styled.div`
position:fixed;
padding:50px;
right:500px;
width:500px;
height:400px;
background: #FFFFFF;
border-radius: 21.0856px;
`

export const Addpara=styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 600;
font-size: 24px;
line-height: 29px;
color: #000000;
`

export const ProdData=styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 600;
font-size: 20px;
line-height: 24px;
color: #000000;

`

export const DiscriInput=styled.input`

    background: #FFFFFF;
    width:500px;
    height:150px;
border: 1px solid rgba(34, 34, 34, 0.15);
border-radius: 12px;
font-family: 'Inter';
font-style: normal;
font-weight: 500;
font-size: 16px;
line-height: 19px;

color: #909090;

`
export const ButtonBoth=styled.div`
display:flex;
justify-content:flex-end;
    margin:10px;
   
`
export const Savebtn=styled.button`
background: #1D2E88;
border-radius: 12px;
color:#ffff;
width:150px;
height:50px;
border:none;

`

export const Cancelbtn=styled.button`
background-color:#ffff;
font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 16px;
line-height: 19px;
border:none;
color: #1D2E88;


`