import { Login } from "components/Login";
import { Dashboard } from "components/Dashboard";
import { UserTable } from "components/UserTable";
// import {Services} from "services/index"

export const Main = () => {
  <div>
    <Login />
    <Dashboard />
    <UserTable />
    {/* <Services/> */}
  </div>;
};
