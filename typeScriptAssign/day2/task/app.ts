// Write the type of response getting from this url

// https://fakestoreapi.com/products/1 

// Write a program to demonstrate all the data types in Typescript.
// Eg. number,string,boolean,object,array,tupple,enum,union types.



const fetchData = async () => {
    try{
        const response = await fetch("https://fakestoreapi.com/products/1");
        let data:{
            id:number,
            title:string,
           price:number,
           description:string,
           category:string,
           image:URL,
           rating:{
                rate:string,
                count:number
            }
           }=
            await response.json()
         console.log(data)           
           
        }

        
    catch(err){
        console.error(err);
    }
}

fetchData();


