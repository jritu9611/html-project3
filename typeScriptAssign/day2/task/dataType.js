// Write a program to demonstrate all the data types in Typescript.
// Eg. number,string,boolean,object,array,tupple,enum,union types.
//number
var num = 2;
console.log(num);
//string
var str = "Hello";
console.log(str);
//boolean
var bool = true;
console.log(bool);
//object
var user = {
    name: "Ritu",
    age: 23,
    id: 1
};
console.log(user);
//Array of String
var arr;
arr = ["a", "sjhd"];
//Array of Object
var arr_names = new Array(4);
for (var i = 0; i < arr_names.length; i++) {
    arr_names[i] = i * 2;
    console.log(arr_names[i]);
}
//tuple
var tupleData;
tupleData = ["ritu", true, 1];
//enum:unchangeable variable
var CardinalDirections;
(function (CardinalDirections) {
    CardinalDirections[CardinalDirections["North"] = 0] = "North";
    CardinalDirections[CardinalDirections["East"] = 1] = "East";
    CardinalDirections[CardinalDirections["South"] = 2] = "South";
    CardinalDirections[CardinalDirections["West"] = 3] = "West";
})(CardinalDirections || (CardinalDirections = {}));
var currentDirection = CardinalDirections.North;
//   currentDirection = 'North'; through error
//union types
function printStatusCode(code) {
    console.log("My status code is ".concat(code, "."));
}
printStatusCode(404);
printStatusCode('404');
