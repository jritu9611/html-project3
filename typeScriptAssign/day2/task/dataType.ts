// Write a program to demonstrate all the data types in Typescript.
// Eg. number,string,boolean,object,array,tupple,enum,union types.
//number
let num:number=2;
console.log(num)
//string
let str:string="Hello"
console.log(str)
//boolean
let bool:boolean=true
console.log(bool)
//object
const user:{
    name:string,
    age:number,
    id:number,
}={ 
    name:"Ritu",
    age:23,
    id:1,
}
console.log(user)
//Array of String

let arr:string[]
arr=["a","sjhd"]
//Array of Object
let arr_names:number[] = new Array(4)  

for(let i = 0;i<arr_names.length;i++) { 
   arr_names[i] = i * 2 
   console.log(arr_names[i]) 
}
//tuple
let tupleData:[string,boolean,number];
tupleData=["ritu",true,1]


//enum:unchangeable variable
enum CardinalDirections {
    North,
    East,
    South,
    West
  }
  let currentDirection = CardinalDirections.North;
//   currentDirection = 'North'; through error


//union types
function printStatusCode(code: string | number) {
    console.log(`My status code is ${code}.`)
  }
  printStatusCode(404);
  printStatusCode('404');
  










