// The program will take color names as input and output a two digit number.
// The band colors are encoded as follows:
//     Black: 0
//     Brown: 1
//     Red: 2
//     Orange: 3
//     Yellow: 4
//     Green: 5
//     Blue: 6
//     Violet: 7
//     Grey: 8
//     White: 9
// From the example above: brown-green should return 15 brown-green-violet should return 15 too, ignoring the third color.
var color = {
    Black: 0,
    Brown: 1,
    Red: 2,
    Orange: 3,
    Yellow: 4,
    Green: 5,
    Blue: 6,
    Violet: 7,
    Grey: 8,
    White: 9
};
function keyValue(str1, str2) {
    // for(const key in color){
    console.log("".concat(color[str1]).concat(color[str2]));
    // break;
    //         if(color[str1]==color[key]){
    //         console.log(`${color[key]}`)
    // }
    // if(color[str2]==color[key]){
    //     console.log(`${color[key]}`)
    // }
}
// }
keyValue("Blue", "Grey");
