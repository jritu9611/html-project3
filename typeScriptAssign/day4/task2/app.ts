type StatusCode =
  | { code: 200, message: string }
  | { code: 300, message: string }
  | { code: 400, message: string }
  | { code: 500, message: string }

const success: StatusCode = { code: 200, message: "OK" }
const warning: StatusCode = { code: 300, message: "Redirect" }
const clientError: StatusCode = { code: 400, message: "Bad Request" }
const serverError: StatusCode = { code: 500, message: "Internal Server Error" }

console.log(success)
