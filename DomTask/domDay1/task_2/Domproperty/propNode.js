const title=document.getElementById('main-heading')
title.style.color='#ffff'
const mainContainer=document.getElementsByClassName('container')
const unorderedList=document.getElementsByTagName('ul')
// for a  single div
const container=document.querySelector('div')
// for all the div if we have more container
// const containerAll=document.querySelectorAll('main-heading')
const listItems=document.querySelectorAll('.list-items')
// listItems[0].style.color='red' for a single list

for(let i=0;i<listItems.length;i++){
    listItems[i].style.border='1px solid '
}

// creating new element
const ul=document.querySelector('ul')
const li=document.createElement('li')
// adding
ul.append(li)
li.innerText='Harry Potter'
li.style.border="1px solid black";

li.classList.add('list-items');
// console.log(li.classList.contains('list-items')) it return true
li.classList.remove('list-items')
// console.log(li.classList.contains('list-items')) it return false
//li.classList.toggle('list-items')


// modifing attributes and classes
li.setAttribute('id','main-heading')
li.removeAttribute('id')
console.log(title.getAttribute('id'))















//* parent node and element traversal

// console.log(ul.parentNode.parentNode)
// console.log(ul.parentElement.parentElement)

// const html=document.documentElement;

// console.log(html.parentNode)
// console.log(html.parentElement)

//child node traversal
// console.log(ul.childNodes)
// console.log(ul.firstChild)
// console.log(ul.lastChild)
//* why we cannot access 2 because at index 0,2,4...we get text
ul.childNodes[3].style.backgroundColor='blue'
// ul.childNodes[2].style.backgroundColor='blue'


//for inside ul element traversal
console.log(ul.children)
console.log(ul.firstElementChild)
console.loglog(ul.lastElementChild)





