let todos = [
    {
        name: "Go to Gym",
        time: "8 Feb, 2023"
    },

    {
        name: "Complete Tasks",
        time: "9 Feb, 2023"
    },
    {
        name: "Read",
        time: "10 Feb, 2023"
    }
];

todos.forEach((todo)=>{
    let divElement = document.createElement('div');
    divElement.setAttribute('class', 'todo');
    let heading = document.getElementById('heading');
    divElement.innerHTML = `<p class="todoName">${todo.name} &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp${todo.time}</p>`;
    heading.after(divElement);
})
