import React, { useState } from "react";
import Welcom from "assets/Images/Welcom.png";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { AiOutlineEye } from "react-icons/ai";
import { AiOutlineEyeInvisible } from "react-icons/ai";
import "custom-input-aslam/build/index.css";
import { NameRejex } from "components/Constant/constant";
import { phoneRegExp } from "components/Constant/constant";
import { EmailRejex } from "components/Constant/constant";
import { Passwordre} from "components/Constant/constant";
import Checkboxbefore from "components/Checkboxbefore";
import Exclude from "assets/Images/Exclude.png";
import {
  Container,
  SignupContainer,
  WelcomeContainer,
  WitsIconImage,
  SignupForm,
  Form,
  FreeTrialSignup,
  FreeTrialTypo,
  FreeTrialNormal,
  LabelInputContainer,
  Label,
  Input,
  PasswordStrengthScroller,
  PasswordStrengthtypo,
  CheckInput,
  CreateAccount,
  AlreadysignupContainer,
  AlreadysignupTypo,
  SigninButton,
  CheckBoxContainer,
  CheckInputLabel,
  WelcomeType,
  WelcomeTypeNormal,
  WelcomeImg,
  Span,
  InputContainer,
  InputSelectContainer,
  Checkmark,
  CheckContainer,
  ScrollerRange,
  ScrollerRange2,
  ScrollerRange3
} from "styles/Signup";
import Wil from "assets/Images/Wil.png";
import { relative } from "path";

const schema = yup
  .object()
  .shape({
    name: yup
      .string()
      .min(3)
      .matches(NameRejex, "It is not a valid format"),
    companyname: yup
      .string()

      .matches(NameRejex, "It is not a valid format"),
    role: yup
      .string()
      .required()
      .matches(NameRejex, "It is not a valid format"),
    email: yup
      .string()
      .required()
      .matches(EmailRejex, "It is not a valid format"),
    phone: yup
      .string()
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10)
      .max(10),
    password: yup.string()
     .matches(Passwordre, "It is not a valid password")
    .required(),
    
  })
  .required();
const Signup = () => {  
  const [type, setType] = useState("password");
  const [minpasswordlength, setMinpasswordLength] = useState(false);
  const [minoneletter, setMinoneLetter] = useState(false);
  const [minonedigit, setMinoneDigit] = useState(false);
  const [password,setPassword]=useState('')
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<any>({resolver: yupResolver(schema)},)
  // <any>({ resolver: yupResolver(schema) });
  const getData = (data: any) => {
  
     console.log(data);
  };


  const handleChange = (value: any) => {
    const letter = new RegExp("^(?=.*[a-zA-Z]).+$");
    const number = new RegExp("(?=.*[0-9])");
    const length = new RegExp("(?=.{8,})");

    if (length.test(value)) {
      setMinpasswordLength(true);
     
    } else {
      setMinpasswordLength(false);
     
    }
    if (letter.test(value)) {
      setMinoneLetter(true);
     
    } else {
      setMinoneLetter(false);
    }
    if (number.test(value)) {
      setMinoneDigit(true);
     
    } else {
      setMinoneDigit(false);
    }
  };

  return (
    <>
      <Container>
        <SignupContainer>
          <WitsIconImage>
            <img src={Wil}></img>
          </WitsIconImage>
          <SignupForm>
            <Form onSubmit={handleSubmit(getData)}>
              <FreeTrialSignup>
                <FreeTrialTypo>Start Your Free Trail</FreeTrialTypo>
                <FreeTrialNormal>
                  Let’s get started with your 7 day trial
                </FreeTrialNormal>
              </FreeTrialSignup>
              <LabelInputContainer>
                <Label>Name*</Label>
                <Input placeholder="Enter Name" {...register("name")}></Input>
                {errors.name?.message && (
                  <Span>{errors?.name?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <LabelInputContainer>
                <Label>Company Name*</Label>
                <Input
                  placeholder="Enter Company Name"
                  {...register("companyname")}
                ></Input>
                {errors.companyname && (
                  <Span>{errors?.companyname?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <LabelInputContainer>
                <Label>Primary Role*</Label>
                <Input
                  placeholder="Enter Primary Role"
                  {...register("role")}
                ></Input>
                {errors.role && (
                  <Span>{errors?.role?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <LabelInputContainer>
                <Label>Email Address*</Label>
                <Input
                  placeholder="Enter Email Address"
                  {...register("email")}
                ></Input>
                {errors.email && (
                  <Span>{errors?.email?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <LabelInputContainer>
                <Label>Phone Number*</Label>
                <InputSelectContainer>
                  <select
                    style={{
                      outline: "none",
                      border: "none",
                      borderRadius: "8px",
                    }}
                  >
                    <option>+91</option>
                  </select>
                  <Input
                    {...register("phone")}
                    style={{ borderStyle: "none" }}
                    placeholder="0000-0000-00"
                  ></Input>
                </InputSelectContainer>
                {errors.phone && (
                  <Span>{errors?.phone?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <LabelInputContainer>
                <Label>Password*</Label>
                <InputContainer>
                  <Input
                    type={type}
                    placeholder="Enter Password"
                    {...register("password")}
                    style={{ borderStyle: "none", outline: "none" }}
                     onChange={(e: any) => {
                       handleChange(e.target.value);
                     }}
                     
                  ></Input>
                  {type === "password" ? (
                    <span
                      onClick={() => {
                        setType("text");
                      }
                    }
                    >
                      <AiOutlineEye
                        style={{ marginTop: "13px", marginRight: "5px" }}
                      />
                    </span>
                  ) : (
                    <span
                      onClick={() => {
                        setType("password");
                      }}
                    >
                      <AiOutlineEyeInvisible
                        style={{ marginTop: "13px", marginRight: "5px" }}
                      />
                    </span>
                  )}
                </InputContainer>
                {errors.password && (
                  <Span>{errors?.password?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <div style={{ position: "relative", height: "15px" }}>
                <div>
                <ScrollerRange
                  minoneletter={minoneletter}
                  minpasswordlength={minpasswordlength}
                  minonedigit={minonedigit}
                />
                <ScrollerRange2   minpasswordlength={minpasswordlength}
                  minonedigit={minonedigit}/>
                <ScrollerRange3   minpasswordlength={minpasswordlength}
                  minonedigit={minonedigit}/>
                <div></div>
                </div>
                <PasswordStrengthScroller></PasswordStrengthScroller>
              </div>
              <PasswordStrengthtypo>Password Strength</PasswordStrengthtypo>
              <CheckBoxContainer>
                {minpasswordlength ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="Circle"
                  />
                ) : (
                  <Checkboxbefore />
                )}
                <CheckInputLabel>Use 8 or more characters.</CheckInputLabel>
              </CheckBoxContainer>
              <CheckBoxContainer>
                {minoneletter ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="logo"
                  ></img>
                ) : (
                  <Checkboxbefore />
                )}
                <CheckInputLabel>Use a minimum of one letter.</CheckInputLabel>
              </CheckBoxContainer>
              <CheckBoxContainer>
                {minonedigit ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="logo"
                  ></img>
                ) : (
                  <Checkboxbefore />
                )}

                <CheckInputLabel>Use a minimum of one number.</CheckInputLabel>
              </CheckBoxContainer>
              <CreateAccount type="submit">Create Account</CreateAccount>
           
              <AlreadysignupContainer>
                <AlreadysignupTypo>Already have an account? </AlreadysignupTypo>
                <SigninButton>Sign In</SigninButton>
              </AlreadysignupContainer>
            </Form>
          </SignupForm>
        </SignupContainer>
        <WelcomeContainer>
          <WelcomeType>
            WELCOME TO <br></br>WIL HRMS
          </WelcomeType>

          <WelcomeTypeNormal>
            Discover the world’s best HRMS tool you will ever need. <br></br>
            Simplest way to manage your workforce.
          </WelcomeTypeNormal>

          <WelcomeImg>
            <img src={Welcom} alt="Welcome"></img>
          </WelcomeImg>
        </WelcomeContainer>
      </Container>
    </>
  );
};

export default Signup;
