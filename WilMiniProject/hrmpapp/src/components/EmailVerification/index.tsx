import React from "react";
import Wil from "assets/Images/Wil.png";
import Message from "assets/Images/Message.png";
import EmailVeri from "assets/Images/EmailVerifi.png";
import Arrow from "assets/Images/Arrow.png"
import { SigninButton } from "styles/Signin";
import { RiArrowDropLeftLine } from 'react-icons/ri';
import {
  EmailVeriConatiner,
  VerificationConatiner,
  VerificationImgConatiner,
  WilImgBackContainer,
  WilIcon,
  VerifyMailContentContainer,
  VerifyMailContent,
  BackButton,
  VerifyMailContentNormal,
  VerifyButton,
  VerifyButtonContainer,
  ImgContainer
} from "styles/EmailVerifi";
const EmailVerification = () => {
  return (
    <EmailVeriConatiner>
      <VerificationConatiner>
        <WilImgBackContainer>
          <WilIcon>
            <img src={Wil} alt="wil"></img>
          </WilIcon>
          <VerifyButtonContainer>
         <RiArrowDropLeftLine size={20}/>
          <BackButton>Back</BackButton>
          </VerifyButtonContainer>
        </WilImgBackContainer>
        <VerifyMailContentContainer>
          <VerifyMailContent>
            <img src={Message} ></img>
          </VerifyMailContent>
          <VerifyMailContent>Verify Your Email</VerifyMailContent>
          <VerifyMailContentNormal>
            We have shared an email verification link to your email.<br></br> Please
            verify you email to continue.
          </VerifyMailContentNormal>
       
         
          <VerifyButton>Resend link</VerifyButton>
        
        </VerifyMailContentContainer>
        <VerifyMailContent></VerifyMailContent>
      </VerificationConatiner>
      <VerificationImgConatiner>
        <ImgContainer><img src={EmailVeri}></img></ImgContainer></VerificationImgConatiner>
    </EmailVeriConatiner>
  );
};

export default EmailVerification;
