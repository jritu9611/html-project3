import React, { useEffect, useState ,useMemo} from "react";
import ReactPaginate from 'react-paginate';
import { useNavigate } from "react-router-dom";
import Wil from "assets/Images/Wil.png";
import SearchIcon from "assets/Images/SearchIcon.png";
import Button from "assets/Images/Button.png";
import Notification from "assets/Images/Notification.png";
import Face from "assets/Images/Face.png";
import DownArrow from "assets/Images/DownArrow.png";
import Collapse from "assets/Images/Collapse.png";
import Dash from "assets/Images/Dashboard.png";
import Inbox from "assets/Images/inbox.png";
import Jobs from "assets/Images/Jobs.png";
import Candidates from "assets/Images/Candidates.png";
import Interviews from "assets/Images/Interviews.png";
import Referral from "assets/Images/Refer.png";
import Department from "assets/Images/Departments.png";
import Campaign from "assets/Images/Campaigns.png";
import Employee from "assets/Images/Employee.png";
import Structure from "assets/Images/Structure.png";
import Team from "assets/Images/Team.png";
import Report from "assets/Images/Reports.png";
import Benefits from "assets/Images/Benefits.png";
import Documents from "assets/Images/Document.png";
import Engage from "assets/Images/Engage.png";
import Tax from "assets/Images/Tax.png";
import PayRoll from "assets/Images/payroll.png";
import OpenJob from "assets/Images/OpenJobs.png";
import CloseJob from "assets/Images/CloseJobs.png";
import HoldJobd from "assets/Images/hand.png";
import DraftIcon from "assets/Images/Drafts.png";
import AllJobs from "assets/Images/AllJobs.png";
import Filter from "assets/Images/Filter.png";
import Candidate from "assets/Images/Candidates.png";
import Edit from "assets/Images/Edit.png";
import Delete from "assets/Images/Delete.png";
import "../pagination.css"

import {
  MainContainer,
  Header,
  DashboardContainer,
  SideNavBar,
  JobOpeningSection,
  JobOpeningHeader,
  JobOpeningData,
  Welcome,
  Name,
  Line,
  SearchContainer,
  SearchInput,
  RightNavbarDiv,
  PlusButton,
  MidNavbarDiv,
  FaceNameContainer,
  CollapseDiv,
  RecruitmentDiv,
  OrganizationDiv,
  Finance,
  NavbarIconDataContainer,
  NavbarIcon,
  NavbarData,
  Heading,
  CreateJob,
  Job,
  JobHeaderTwo,
  MyOpenJobs,
  SearchConatiner,
  SearchDiv,
  StatusJobContainer,
  TitleIconContainer,
  JobStatusIcon,
  JobStatusTitle,
  FilterButton,
  SearchFilterContainer,
  SearchTitle,
  FilterTitle,
  TableRow,
  TableData,
  TableRowConatiner,
  Table,
  Tr,
  Th,
  Td,
  EditDeleteContainer,
  Pagination,
} from "styles/dashboard";
import { Data } from "components/Constant/data";

const Dashboard = () => {
  
  

  // const {pages,nbPages}=useGlobalContext()
  const [activeButton, setActiveButton] = useState<any>(false);
  const [activeButtonHeader, setActiveButtonHeader] = useState<any>(false);
  const [openAlldata,setAllStatus]=useState<any>(Data)
  const [openStatusdata,setOpenStatus]=useState<any>([])
  const [closeStatusdata,setCloseStatus]=useState<any>([])
  const [holdStatusdata,setHoldStatus]=useState<any>([])
  const [draftStatusdata,setDraftStatus]=useState<any>([])
 const navigate=useNavigate()


 const AllData=()=>{
  
  setActiveButtonHeader("headerbutton1")
  setAllStatus(Data)
  setOpenStatus([])
  setCloseStatus([])
  setHoldStatus([])
  setDraftStatus([])
  
}


  const openStatus=()=>{
    setActiveButtonHeader("headerbutton2")
    const OpenData=Data.filter((items:any)=>items.Status==="Open")
    console.log(OpenData)
    
    setOpenStatus(OpenData)
    setAllStatus([])
    setCloseStatus([])
    setHoldStatus([])
    setDraftStatus([])
 
  }


  const closeStatus=()=>{
    setActiveButtonHeader("headerbutton3")
    console.log("close")
    const closeData=Data.filter((items:any)=>items.Status==="Close")
    console.log(closeData)
    setCloseStatus(closeData)
    setOpenStatus([])
    setAllStatus([])
    setHoldStatus([])
    setDraftStatus([])
    
  }


  const holdStatus=()=>{
    setActiveButtonHeader("headerbutton4")
    console.log("hold")
    const HoldData=Data.filter((items:any)=>items.Status==="Hold")
    console.log(HoldData)
   
    setAllStatus([])
    setOpenStatus([])
    setCloseStatus([])
    setHoldStatus(HoldData)
    setDraftStatus([])
   
  }



  const draftStatus=()=>{
    setActiveButtonHeader("headerbutton5")
    console.log("draft")
    const draftData=Data.filter((items:any)=>items.Status==="Draft")
    console.log(draftData)
    setDraftStatus(draftData)
   
    setAllStatus([])
    setOpenStatus([])
    setCloseStatus([])
    setHoldStatus([])
   
    
  }

  const [data, setData] = useState<any>([]); // Data obtained in object format
  const [currentPage, setCurrentPage] = useState<number>(0); // Current page number
  const itemsPerPage = 5; // Number of items to display per page
  const handlePageChange = (selectedPage: { selected: number }) => {
    setCurrentPage(selectedPage.selected);
  };
  // Calculate the start and end index of the data for the current page
  const startIndex = currentPage * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const paginatedData = data.slice(startIndex, endIndex);
  const fetchData = () => {
    setData(Data);
    setCloseStatus([]);
    setHoldStatus([]);
    setAllStatus([]);
  };

  return (
    <>
      <MainContainer>
        <Header>
          <img src={Wil} alt="logo"></img>
          <MidNavbarDiv>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <Welcome>Welcome,</Welcome>
              <Name>Ritu</Name>
            </div>
            <Line></Line>
            <SearchContainer>
              <img src={SearchIcon} alt="logo"></img>
              <SearchInput>Search...</SearchInput>
            </SearchContainer>
          </MidNavbarDiv>
          <RightNavbarDiv>
            <img
              src={Button}
              style={{ width: "36px", height: "36px" }}
              alt="logo"
            ></img>
            <img
              src={Notification}
              style={{ width: "18px", height: "20px" }}
              alt="logo"
            ></img>
            <FaceNameContainer>
              <img
                src={Face}
                style={{ width: "32px", height: "32px", borderRadius: "50%" }}
                alt="logo"
              ></img>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <Welcome style={{ fontSize: "14px", lineHeight: "10px" }}>
                  Priyanka kapoor,
                </Welcome>
                <Name>Admin</Name>
              </div>
            </FaceNameContainer>
            <img
              src={DownArrow}
              style={{ width: "16px", height: "9px" }}
              alt="logo"
            ></img>
          </RightNavbarDiv>
        </Header>
        <DashboardContainer>
          <SideNavBar>
            <CollapseDiv>
              <NavbarIconDataContainer
                style={{
                  width: "180px",
                  height: "44px",
                  backgroundColor: "#4D5CAD",
                  borderRadius: "8px",
                  padding: "10px",
                }}
              >
                <NavbarIcon>
                  <img src={Collapse}></img>
                </NavbarIcon>
                <NavbarData>Collapse</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                style={{ height: "40px" }}
              ></NavbarIconDataContainer>
              <div style={{ padding: "10px" }}>
                <NavbarIconDataContainer
                  className={activeButton === "button1" ? "activeButton" : ""}
                  onClick={() => setActiveButton("button1")}
                  
                  style={{
                    backgroundColor:
                      activeButton === "button1" ? "#F9C51C" : "",
                  }}
                >
                  <NavbarIcon >
                   <img src={Dash}></img>
                  </NavbarIcon>
                  <NavbarData
                 
                  
                    style={{
                      color: activeButton === "button1" ? "black" : "",
                    }}
                  >
                    Dashboard
                  </NavbarData>
                </NavbarIconDataContainer>
                <NavbarIconDataContainer
                  className={activeButton === "button2" ? "activeButton" : ""}
                  onClick={() => setActiveButton("button2")}
                  style={{
                    backgroundColor:
                      activeButton === "button2" ? "#F9C51C" : "",
                  }}
                >
                  <NavbarIcon>
                    <img src={Inbox}></img>
                  </NavbarIcon>
                  <NavbarData style={{
                      color: activeButton === "button2" ? "black" : "",
                    }}>Inbox</NavbarData>
                </NavbarIconDataContainer>
              </div>
            </CollapseDiv>

            <Heading>RECRUITMENT</Heading>

            <RecruitmentDiv>
              <NavbarIconDataContainer
                className={activeButton === "button3" ? "activeButton" : ""}
                onClick={() => setActiveButton("button3")}
                style={{
                  backgroundColor: activeButton === "button3" ? "#F9C51C" : "",
                
                }}
               
              >
                <NavbarIcon>
                  <img src={Jobs}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button3" ? "black" : "",
                    }}>Jobs</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className="button4"
                onClick={() => setActiveButton("button4")}
                style={{
                  backgroundColor: activeButton === "button4" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Candidates}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button4" ? "black" : "",
                    }}>Candidates</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button5" ? "activeButton" : ""}
                onClick={() => setActiveButton("button5")}
                style={{
                  backgroundColor: activeButton === "button5" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Interviews}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button5" ? "black" : "",
                    }}>Interviews</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button6" ? "activeButton" : ""}
                onClick={() => setActiveButton("button6")}
                style={{
                  backgroundColor: activeButton === "button6" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Referral}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button6" ? "black" : "",
                    }}>Referrals</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button7" ? "activeButton" : ""}
                onClick={() => setActiveButton("button7")}
                style={{
                  backgroundColor: activeButton === "button7" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Department}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button7" ? "black" : "",
                    }}>Departments</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button8" ? "activeButton" : ""}
                onClick={() => setActiveButton("button8")}
                style={{
                  backgroundColor: activeButton === "button8" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Campaign}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button8" ? "black" : "",
                    }}>Campaigns</NavbarData>
              </NavbarIconDataContainer>
            </RecruitmentDiv>
            <Heading>ORGANISATION</Heading>
            <OrganizationDiv>
              <NavbarIconDataContainer
                className={activeButton === "button9" ? "activeButton" : ""}
                onClick={() => setActiveButton("button9")}
                style={{
                  backgroundColor: activeButton === "button9" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Employee}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button9" ? "black" : "",
                    }}>Employee</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button16" ? "activeButton" : ""}
                onClick={() => setActiveButton("button16")}
                style={{
                  backgroundColor: activeButton === "button16" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Structure}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button16" ? "black" : "",
                    }}>Structure</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button10" ? "activeButton" : ""}
                onClick={() => setActiveButton("button10")}
                style={{
                  backgroundColor: activeButton === "button10" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Team}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button10" ? "black" : "",
                    }}>Team Insights</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button11" ? "activeButton" : ""}
                onClick={() => setActiveButton("button11")}
                style={{
                  backgroundColor: activeButton === "button11" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Report}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button11" ? "black" : "",
                    }}>Reports</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className="button12"
                onClick={() => setActiveButton("button12")}
                style={{
                  backgroundColor: activeButton === "button12" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Benefits}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button12" ? "black" : "",
                    }}>Benefits</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button13" ? "activeButton" : ""}
                onClick={() => setActiveButton("button13")}
                style={{
                  backgroundColor: activeButton === "button13" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Documents}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button13" ? "black" : "",
                    }}>Documents</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer
                className={activeButton === "button14" ? "activeButton" : ""}
                onClick={() => setActiveButton("button14")}
                style={{
                  backgroundColor: activeButton === "button14" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Engage}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button14" ? "black" : "",
                    }}>Engage</NavbarData>
              </NavbarIconDataContainer>
            </OrganizationDiv>
            <Finance>
              <Heading>FINANCES</Heading>
              <NavbarIconDataContainer
                className={activeButton === "button15" ? "activeButton" : ""}
                onClick={() => setActiveButton("button15")}
                style={{
                  backgroundColor: activeButton === "button15" ? "#F9C51C" : "",
                }}
              >
                <NavbarIcon>
                  <img src={Tax}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button15" ? "black" : "",
                    }}>Tax Management</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer  className={activeButton === "button17" ? "activeButton" : ""}
                onClick={() => setActiveButton("button17")}
                style={{
                  backgroundColor: activeButton === "button17" ? "#F9C51C" : "",
                }}>
                <NavbarIcon>
                  <img src={PayRoll}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button17" ? "black" : "",
                    }}>Payroll</NavbarData>
              </NavbarIconDataContainer>
              <NavbarIconDataContainer  className={activeButton === "button18" ? "activeButton" : ""}
                onClick={() => setActiveButton("button18")}
                style={{
                  color: activeButton === "button18" ? "#101010" : "",
                }}>
                <NavbarIcon>
                  <img src={Documents}></img>
                </NavbarIcon>
                <NavbarData style={{
                      color: activeButton === "button18" ? "black" : "",
                    }}>Documents</NavbarData>
              </NavbarIconDataContainer>
            </Finance>
          </SideNavBar>
          <JobOpeningSection>
            <JobOpeningHeader>
              <Job>Jobs</Job>
              <CreateJob>Create Job Opening</CreateJob>
            </JobOpeningHeader>

            <JobOpeningData>
              <StatusJobContainer>
                <TitleIconContainer onClick={()=>{AllData()}}
                  className={activeButtonHeader === "headerbutton1" ? "activeButtonHeader" : ""} 
                  style={{ borderBottom:activeButtonHeader==="headerbutton1" ? "3px solid #1D2E88" : ""}}
                >
                  <JobStatusIcon >
                    <img src={Jobs}></img>
                  </JobStatusIcon>
                  <JobStatusTitle   
                  style={{
                  color:
                   activeButtonHeader === "headerbutton1" ? "#1D2E88" : "",
                  
                 
                   }}
                    >All Jobs</JobStatusTitle>
                </TitleIconContainer>

                <TitleIconContainer   className={activeButtonHeader === "headerbutton2" ? "activeButtonHeader" : ""}
                  onClick={()=>{openStatus()}}  style={{
                    borderBottom:
                     activeButtonHeader === "headerbutton2" ? "3px solid #1D2E88" : "",
                       }}>
                  <JobStatusIcon>
                    <img src={OpenJob}></img>
                  </JobStatusIcon>
                  <JobStatusTitle  
                 style={{
                color:
                 activeButtonHeader === "headerbutton2" ? "#1D2E88" : "",
                   }}>My Open Jobs</JobStatusTitle>
                </TitleIconContainer>

                <TitleIconContainer onClick={()=>{closeStatus()}}
                  
                  className={activeButtonHeader === "headerbutton3" ? "activeButtonHeader" : ""}
                  style={{
                    borderBottom:
                     activeButtonHeader === "headerbutton3" ? "3px solid #1D2E88" : "",
                       }}
                > 
                  <JobStatusIcon>
                    <img src={CloseJob}></img>
                  </JobStatusIcon>
                  <JobStatusTitle 
                  style={{
                color:
                 activeButtonHeader === "headerbutton3" ? "#1D2E88" : "",
                   }}>My Closed Jobs</JobStatusTitle>
                </TitleIconContainer>

                <TitleIconContainer onClick={holdStatus}  className={activeButtonHeader === "headerbutton4" ? "activeButtonHeader" : ""}
                  style={{
                    borderBottom:
                     activeButtonHeader === "headerbutton4" ? "3px solid #1D2E88" : "",
                       }}>
                  <JobStatusIcon>
                    <img src={HoldJobd}></img>
                  </JobStatusIcon>
                  <JobStatusTitle style={{
                color:
                 activeButtonHeader === "headerbutton4" ? "#1D2E88" : "",
                   }} >My On-Hold Jobs</JobStatusTitle>
                </TitleIconContainer>

                <TitleIconContainer  onClick={draftStatus} className={activeButtonHeader === "headerbutton5" ? "activeButtonHeader" : ""}
                  style={{
                    borderBottom:
                     activeButtonHeader === "headerbutton5" ? "3px solid #1D2E88" : "",
                       }}>
                  <JobStatusIcon>
                    <img src={DraftIcon}></img>
                  </JobStatusIcon>
                  <JobStatusTitle style={{
                color:
                 activeButtonHeader === "headerbutton5" ? "#1D2E88" : "",
                   }}>My Drafts</JobStatusTitle>
                </TitleIconContainer>
              </StatusJobContainer>
              <JobHeaderTwo>
                <MyOpenJobs>All Jobs</MyOpenJobs>
                <SearchFilterContainer>
                  <SearchConatiner>
                    <img src={SearchIcon} alt="logo"></img>
                    <SearchTitle>Search Jobs</SearchTitle>
                  </SearchConatiner>

                  <FilterButton>
                    <img src={Filter}></img>
                    <FilterTitle>Filter</FilterTitle>
                  </FilterButton>
                </SearchFilterContainer>
              </JobHeaderTwo>
              <Table>
                <Tr>
                  <Th>Job Title</Th>
                  <Th>Created</Th>
                  <Th>Openings</Th>
                  <Th>Candidates</Th>
                  <Th>Hiring Lead</Th>
                  <Th>Assigned Timeline</Th>
                  <Th>Status</Th>
                  <Th>Action</Th>
                </Tr>

                {openAlldata.map((item: any) => (
                  <Tr>
                    <Td>{item.JobTitile}</Td>
                    <Td>{item.Created}</Td>
                    <Td>{item.Openings}</Td>
                    <Td>{item.Candidated}</Td>
                    <Td>{item.HiringLead}</Td>
                    <Td>{item.AssignedTimeline}</Td>
                    <Td>{item.Status}</Td>
                    <Td>
                      <div style={{ display: "flex", paddingLeft: "70px" }}>
                        <EditDeleteContainer>
                          <img src={Edit} alt="logo"></img>
                        </EditDeleteContainer>
                        <EditDeleteContainer>
                          <img src={Delete} alt="logo"></img>
                        </EditDeleteContainer>
                      </div>
                    </Td>
                  </Tr>
                ))}

               {openStatusdata.map((item: any) => (
                  <Tr>
                    <Td>{item.JobTitile}</Td>
                    <Td>{item.Created}</Td>
                    <Td>{item.Openings}</Td>
                    <Td>{item.Candidated}</Td>
                    <Td>{item.HiringLead}</Td>
                    <Td>{item.AssignedTimeline}</Td>
                    <Td>{item.Status}</Td>
                    <Td>
                      <div style={{ display: "flex", paddingLeft: "70px" }}>
                        <EditDeleteContainer>
                          <img src={Edit} alt="logo"></img>
                        </EditDeleteContainer>
                        <EditDeleteContainer>
                          <img src={Delete} alt="logo"></img>
                        </EditDeleteContainer>
                      </div>
                    </Td>
                  </Tr>
                ))}
                {closeStatusdata.map((item: any) => (
                  <Tr>
                    <Td>{item.JobTitile}</Td>
                    <Td>{item.Created}</Td>
                    <Td>{item.Openings}</Td>
                    <Td>{item.Candidated}</Td>
                    <Td>{item.HiringLead}</Td>
                    <Td>{item.AssignedTimeline}</Td>
                    <Td>{item.Status}</Td>
                    <Td>
                      <div style={{ display: "flex", paddingLeft: "70px" }}>
                        <EditDeleteContainer>
                          <img src={Edit} alt="logo"></img>
                        </EditDeleteContainer>
                        <EditDeleteContainer>
                          <img src={Delete} alt="logo"></img>
                        </EditDeleteContainer>
                      </div>
                    </Td>
                  </Tr>
                ))}
                {holdStatusdata.map((item: any) => (
                  <Tr>
                    <Td>{item.JobTitile}</Td>
                    <Td>{item.Created}</Td>
                    <Td>{item.Openings}</Td>
                    <Td>{item.Candidated}</Td>
                    <Td>{item.HiringLead}</Td>
                    <Td>{item.AssignedTimeline}</Td>
                    <Td>{item.Status}</Td>
                    <Td>
                      <div style={{ display: "flex", paddingLeft: "70px" }}>
                        <EditDeleteContainer>
                          <img src={Edit} alt="logo"></img>
                        </EditDeleteContainer>
                        <EditDeleteContainer>
                          <img src={Delete} alt="logo"></img>
                        </EditDeleteContainer>
                      </div>
                    </Td>
                  </Tr>
                ))}
                 {draftStatusdata.map((item: any) => (
                  <Tr>
                    <Td>{item.JobTitile}</Td>
                    <Td>{item.Created}</Td>
                    <Td>{item.Openings}</Td>
                    <Td>{item.Candidated}</Td>
                    <Td>{item.HiringLead}</Td>
                    <Td>{item.AssignedTimeline}</Td>
                    <Td>{item.Status}</Td>
                    <Td>
                      <div style={{ display: "flex", paddingLeft: "70px" }}>
                        <EditDeleteContainer>
                          <img src={Edit} alt="logo"></img>
                        </EditDeleteContainer>
                        <EditDeleteContainer>
                          <img src={Delete} alt="logo"></img>
                        </EditDeleteContainer>
                      </div>
                    </Td>
                  </Tr>
                ))}
              </Table>
              
             {/* <Pagination>

              <button onClick={()=>{}}></button>
             </Pagination> */}
            </JobOpeningData>
          </JobOpeningSection>
        </DashboardContainer>
      </MainContainer>
    </>
  );
};

export default Dashboard;
