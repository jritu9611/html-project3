import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Wil from "assets/Images/Wil.png";
import Welcom from "assets/Images/Welcom.png";
import Password from "assets/Images/Password.png"
import{ ISigninData} from "interfaces/signin"
import {Span} from "styles/Signup"
import { AiOutlineEye } from "react-icons/ai";
import { AiOutlineEyeInvisible } from "react-icons/ai"; 


import {
  Container,
  SigninPage,
  WelcomeSignupPage,
  WilIcon,
  FormContainer,
  Form,
  SigninTypewriter,
  NormalTypeWriter,
  Label,
  Input,
  ForgetPass,
  SigninButton,
  NoAccount,
  LabelInputContainer,
  FreeTrial,
  NoCredencial,
  SigninNomalContainer,
  WelcomeTypo,
  WelcomeNormalTypo,
  WelcomeImage,
  WelcomeNormalContainer,
  PassImgContainer,
  
} from "styles/Signin";


const schema = yup
  .object()
  .shape({
    email: yup.string().required(),
    password: yup.string().required(),
  })
  .required();


const Signin = () => {

  const { register, handleSubmit, watch, formState: { errors } } = useForm<ISigninData>({resolver: yupResolver(schema)},);

  const getData=(data:ISigninData)=>{
  console.log(data)
  
  }


  const [type,setType]=useState("password")
  return (
  
      <Container>
        <SigninPage>
          <WilIcon>
            <img src={Wil}></img>
          </WilIcon>
          <FormContainer>
            <Form onSubmit={handleSubmit(getData)}>
              <SigninNomalContainer>
              <SigninTypewriter>Sign In</SigninTypewriter>
              <NormalTypeWriter>Amet minim mollit non deserunt ullamco.</NormalTypeWriter>
              </SigninNomalContainer>
              <LabelInputContainer>
              <Label>Email Address*</Label>
              <Input placeholder="Enter Email Address" {...register("email")}></Input>
              {errors.email && <Span>{errors.email?.message}</Span>}
              </LabelInputContainer>
              <LabelInputContainer>
              <Label>Password*</Label>
              <PassImgContainer>
              <Input placeholder="Enter Password" style={{borderStyle:"none",minHeight:"30px"}} {...register("password")} type={type}/>
              {type==="password"?(<span onClick={()=>{
                setType("text")
              }}><AiOutlineEye     style={{ marginTop: "13px", marginRight: "5px" }}/></span>):(<span onClick={()=>setType("password")}><AiOutlineEyeInvisible     style={{ marginTop: "13px", marginRight: "5px" }}/></span>)}
            
              </PassImgContainer>
              {errors.password && <Span>{errors.password?.message}</Span>}
              </LabelInputContainer>
              <ForgetPass>Forgot Password?</ForgetPass>
              <SigninButton>Sign In</SigninButton>
  <NoCredencial>
              <NoAccount>Don’t have an account? </NoAccount>
              <FreeTrial>Start Your Free Trial</FreeTrial>
              </NoCredencial>
            </Form>
          </FormContainer>
        </SigninPage>
        <WelcomeSignupPage>
          
            <WelcomeNormalContainer>
            <WelcomeTypo>Welcome Back!</WelcomeTypo>
            <WelcomeNormalTypo>Log in to your account - enjoy exclusive features and <br></br> much more.</WelcomeNormalTypo>
            </WelcomeNormalContainer>
            <WelcomeImage><img src={Welcom} style={{maxWidth:"100%",height:"auto"}} alt=""></img></WelcomeImage>
            
        </WelcomeSignupPage>
      </Container>
   
  );
};

export default Signin;
