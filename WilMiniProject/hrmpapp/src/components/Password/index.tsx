
import  { Component } from 'react';
import React, { useState } from "react";
import Welcom from "assets/Images/Welcom.png";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import zxcvbn from "zxcvbn"
import * as yup from "yup";
import { AiOutlineEye } from "react-icons/ai";
import { AiOutlineEyeInvisible } from "react-icons/ai";
import "custom-input-aslam/build/index.css";
import { NameRejex } from "components/Constant/constant";
import { phoneRegExp } from "components/Constant/constant";
import { EmailRejex } from "components/Constant/constant";
import { Passwordre} from "components/Constant/constant";
import Checkboxbefore from "components/Checkboxbefore";
import Exclude from "assets/Images/Exclude.png";
import {
  Container,
  SignupContainer,
  LabelInputContainer,
  Label,
  Input,
  PasswordStrengthScroller,
  PasswordStrengthtypo,
  CheckInput,
  CreateAccount,
  AlreadysignupContainer,
  AlreadysignupTypo,
  SigninButton,
  CheckBoxContainer,
  CheckInputLabel,
  WelcomeType,
  WelcomeTypeNormal,
  WelcomeImg,
  Span,
  InputContainer,
  InputSelectContainer,
  Checkmark,
  CheckContainer,
  ScrollerRange,
  ScrollerRange2,
  ScrollerRange3
} from "styles/Password";
import Wil from "assets/Images/Wil.png";
import { relative } from "path";

const schema = yup
  .object()
  .shape({
    name: yup
      .string()
      .min(3)
      .matches(NameRejex, "It is not a valid format"),
    companyname: yup
      .string()

      .matches(NameRejex, "It is not a valid format"),
    role: yup
      .string()
      .required()
      .matches(NameRejex, "It is not a valid format"),
    email: yup
      .string()
      .required()
      .matches(EmailRejex, "It is not a valid format"),
    phone: yup
      .string()
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10)
      .max(10),
    password: yup.string()
     .matches(Passwordre, "It is not a valid password")
    .required(),
    
  })
  .required();
  
const Password =()=>  {  
  const [counter, setCounter] = useState(0);

  const [type, setType] = useState("password");
  const [minpasswordlength, setMinpasswordLength] = useState(false);
  const [minoneletter, setMinoneLetter] = useState(false);
  const [minonedigit, setMinoneDigit] = useState(false);
  const [password,setPassword]=useState('')
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<any>({resolver: yupResolver(schema)},)
  // <any>({ resolver: yupResolver(schema) });
  const getData = (data: any) => {
  
     console.log(data);
  };

  


  const handleChange = (value: any) => {
    
  };

  return (
    <>
      <Container>
        <SignupContainer>
         
              <LabelInputContainer>
                <Label>Password*</Label>
                <InputContainer>
                  <Input
                    type={type}
                    placeholder="Enter Password"
                    {...register("password")}
                    style={{ borderStyle: "none", outline: "none" }}
                     onChange={(e: any) => {
                      setCounter(counter + 1);
                       handleChange(e.target.value);
                       const password=e.target.value

                       const evolution=zxcvbn(password)
                     
                       const StrengthData=evolution.score
                      console.log(StrengthData)
                     }}
                     
                  ></Input>
                  {type === "password" ? (
                    <span
                      onClick={() => {
                        setType("text");
                      }
                    }
                    >
                      <AiOutlineEye
                        style={{ marginTop: "13px", marginRight: "5px" }}
                      />
                    </span>
                  ) : (
                    <span
                      onClick={() => {
                        setType("password");
                      }}
                    >
                      <AiOutlineEyeInvisible
                        style={{ marginTop: "13px", marginRight: "5px" }}
                      />
                    </span>
                  )}
                </InputContainer>
                {errors.password && (
                  <Span>{errors?.password?.message?.toString()}</Span>
                )}
              </LabelInputContainer>
              <div style={{ position: "relative", height: "15px" }}>
                <div>
                <ScrollerRange
                
                  
                />
                <ScrollerRange2   
                 />
                <ScrollerRange3   
                 
                  />
                <div></div>
                </div>
                <PasswordStrengthScroller></PasswordStrengthScroller>
              </div>
              <PasswordStrengthtypo>Password Strength</PasswordStrengthtypo>
              <CheckBoxContainer>
                {minpasswordlength ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="Circle"
                  />
                ) : (
                  <Checkboxbefore />
                )}
                <CheckInputLabel>Use 8 or more characters.</CheckInputLabel>
              </CheckBoxContainer>
              <CheckBoxContainer>
                {minoneletter ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="logo"
                  ></img>
                ) : (
                  <Checkboxbefore />
                )}
                <CheckInputLabel>Use a minimum of one letter.</CheckInputLabel>
              </CheckBoxContainer>
              <CheckBoxContainer>
                {minonedigit ? (
                  <img
                    src={Exclude}
                    style={{
                      width: "14px",
                      height: "14px",
                      marginTop: "5px",
                      marginRight: "3px",
                    }}
                    alt="logo"
                  ></img>
                ) : (
                  <Checkboxbefore />
                )}

                <CheckInputLabel>Use a minimum of one number.</CheckInputLabel>
              </CheckBoxContainer>
              <CreateAccount type="submit">Create Account</CreateAccount>
           
             
       
        </SignupContainer>
       
      </Container>
    </>
  );
};

export default Password;
