import styled from "styled-components";

export const CheckboxWrapper = styled.label`
  width:14px;
  height:14px;
  background-color:#F5F5F5;
  border:1px solid black;
  border-radius:50%;
  margin-top:5px;
  margin-right:3px;
`;

export const CheckboxInput = styled.input`
  
`;

export const CheckboxIcon = styled.span`
  
`;

export const CheckboxText = styled.span`
  font-size: 14px;
 `;
