import styled from "styled-components";


export const EmailVeriConatiner=styled.div`
width:100%;
height:100vh;
display:flex;

`

export const VerificationConatiner=styled.div`
width:55%;
@media (max-width:826px) {
    width:100%;
}
`


export const WilImgBackContainer=styled.div`
display:flex;
justify-content:space-between;
margin:60px;
`
export const WilIcon=styled.div`

`
export const VerifyMailContentContainer=styled.div`
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
margin-top:200px;
`

export const VerifyMailContent=styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 32px;
line-height: 40px;
/* identical to box height, or 125% */

display: flex;
align-items: center;

/* Neutral/neutral black-500 */

color: #1D1D1D;

`

export const VerifyMailContentNormal=styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;
/* or 150% */

display: flex;
align-items: center;
text-align: center;

/* Neutral/neutral black-400 */

color: #4A4A4A;


`

export const BackButton=styled.button`
border-style:none;
background-color:transparent;
color: #1D2E88;

`

export const VerifyButton=styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 16px;
gap: 8px;
margin-top:20px;
width: 428px;
height: 56px;

/* Blue/primary blue-600 */

background: #1D2E88;
border-radius: 8px;
color:#fff;

@media (max-width:433px) {
    width: 308px;
}
`
export const VerifyButtonContainer=styled.div`
box-sizing: border-box;

/* Auto layout */
margin-top:20px;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
width: 79px;
height: 30px;

/* Blue/primary blue-600 */
background-color:transparent;
color: #1D2E88;

border: 2px solid #1D2E88;
border-radius: 10px;

`


export const VerificationImgConatiner=styled.div`
width:45%;
display:flex;

justify-content:center;
align-items:center;
background-color:#1D2E88;
@media (max-width:826px) {
    display:none;
}

`

export const ImgContainer=styled.div`
width:100%;
height:auto;
display:flex;
justify-content:center;

`
