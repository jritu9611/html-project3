import styled from "styled-components";



export const Container = styled.div`
  width: 100%;
  display: flex;
  min-height: 100vh;
`;

export const SignupContainer = styled.div`
  width: 53%;
  /* min-height:100%; */
  margin-bottom: 60px;

  @media (max-width: 1030px) {
    width: 100%;
  }
`;

export const WitsIconImage = styled.div`
  padding: 60px 0px 40px 60px;
`;

export const SignupForm = styled.div`
  display: flex;
  justify-content: center;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  @media (max-width: 480px) {
    align-items: center;
  }
`;
export const FreeTrialSignup = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 20px;
`;

export const FreeTrialTypo = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 700;
  font-size: 32px;
  line-height: 40px;
  /* identical to box height, or 125% */

  display: flex;
  align-items: center;

  /* Neutral/neutral black-500 */

  color: #1d1d1d;
`;

export const FreeTrialNormal = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height, or 150% */

  display: flex;
  align-items: center;

  /* Neutral/neutral black-400 */

  color: #4a4a4a;
`;

export const LabelInputContainer = styled.div`
  margin-top: 13px;
  height: 90px;
`;

export const Label = styled.label`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;

  /* identical to box height, or 129% */

  display: flex;
  align-items: center;

  /* Neutral/neutral black-500 */

  color: #1d1d1d;
`;

export const Input = styled.input`
  box-sizing: border-box;

  outline: none;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding-left: 10px;

  width: 428px;
  height: 44px;

  background: #ffffff;

  border: 1px solid #b9b9b9;
  border-radius: 8px;
  @media (max-width: 480px) {
    width: 300px;
  }
`;
export const ScrollerRange = styled.div`
  position: absolute;
  top: 10;
  border: none;
  height: 6px;
  background-color: #f9c51c;
  z-index: 4;

  
`;

export const ScrollerRange2=styled.div`
position: absolute;
  top: 10;
  border: none;
  height: 6px;
  background-color: #f9c51c;
  z-index: 4;

  

`

export const ScrollerRange3=styled.div`
position: absolute;
  top: 10;
  border: none;
  height: 6px;
  background-color: green;
  z-index: 4;

  
 
`


export const PasswordStrengthScroller = styled.div`
  top: 10;
  margin-top: 2px;
  margin-bottom: 10px;
  width: 428px;
  height: 4px;

  /* Neutral/200 */

  background: #e2e8f0;
  border-radius: 10px;

  /* Inside auto layout */

  flex: none;
  order: 0;
  align-self: stretch;
  flex-grow: 0;
  @media (max-width: 480px) {
    width: 300px;
  }
`;

export const PasswordStrengthtypo = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  /* identical to box height, or 143% */

  display: flex;
  align-items: center;

  color: #282828;
`;

export const CreateAccount = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 16px;
  gap: 8px;

  width: 428px;
  height: 56px;
  margin-top: 10px;
  /* Blue/primary blue-600 */
  color: #fff;
  background: #1d2e88;
  border-radius: 8px;
  @media (max-width: 480px) {
    width: 300px;
  }
`;

export const AlreadysignupContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 428px;
  padding: 10px;
`;
export const AlreadysignupTypo = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  /* identical to box height, or 133% */

  display: flex;
  align-items: center;

  /* Neutral/600 */

  color: #475569;
`;
export const SigninButton = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  /* identical to box height, or 143% */

  /* Blue/primary blue-600 */

  color: #1d2e88;
`;

export const WelcomeContainer = styled.div`
  width: 47%;
  /* height:100vh; */

  background-color: #1d2e88;
  /* display:flex; */
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 80px 0 0 80px;

  @media (max-width: 1030px) {
    display: none;
  }
`;

export const WelcomeType = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 700;
  font-size: 42px;
  line-height: 60px;
  /* or 125% */

  display: flex;
  align-items: center;

  color: #ffffff;
`;

export const WelcomeTypeNormal = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* or 150% */

  display: flex;
  align-items: center;

  color: #ffffff;

  /* Inside auto layout */

  flex: none;
  order: 1;
  flex-grow: 0;
`;

export const WelcomeImg = styled.div`
  margin-top: 108.5px;
`;

export const Span = styled.span`
  color: red;
  padding-bottom: 20px;
`;

export const InputContainer = styled.div`
  box-sizing: border-box;

  display: flex;
  width: 428px;

  background: #ffffff;

  border: 1px solid #b9b9b9;
  border-radius: 8px;

  @media (max-width: 480px) {
    width: 300px;
  }
`;

export const InputSelectContainer = styled.div`
  box-sizing: border-box;

  /* Auto layout */

  display: flex;
  width: 428px;

  /* Basic/white */
  margin-bottom: 10px;
  background: #ffffff;
  /* Neutral/neutral black-100 */
  outline: none;
  border: 1px solid #b9b9b9;
  border-radius: 8px;
  @media (max-width: 480px) {
    width: 300px;
  }
`;

export const CheckBoxContainer = styled.div`
  display: flex;
`;

export const CheckContainer = styled.div``;

export const CheckInput = styled.input``;

export const Checkmark = styled.span``;

export const CheckInputLabel = styled.label``;


