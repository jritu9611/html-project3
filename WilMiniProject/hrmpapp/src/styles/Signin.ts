import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  max-width: 100%;

  height: 100vh;
  @media (max-width:848px) {
    display:flex;
    flex-direction:column-reverse;
  
  }
`;

export const SigninPage = styled.div`
  display: flex;
  flex-direction: column;
  width: 55%;
  height: 100vh;
  background-color: #f5f5f5;
  @media (max-width:848px) {
    display:flex;
    width: 100%;
    min-height:100vh;
    flex-shrink:1;
    flex-grow:1;
  }
`;



export const WilIcon = styled.div`
  margin: 60px 0 0 80px;
`;

export const FormContainer = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-width: 428px;
  min-height: 434px;
`;

export const SigninNomalContainer=styled.div`
display:flex;
flex-direction:column;


`
export const SigninTypewriter = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 700;
  font-size: 32px;

  color: #1d1d1d;
`;

export const NormalTypeWriter = styled.div`
  max-width: 428px;
  min-height: 24px;

  /* Body 1/Regular */

  font-style: normal;
  font-weight: 400;
  font-size: 16px;

  /* identical to box height, or 150% */

  /* Neutral/neutral black-400 */

  color: #4a4a4a;

  /* Inside auto layout */
`;

export const LabelInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-height:60px;
`;

export const Label = styled.label`
  max-width: 428px;
  min-height: 18px;

  /* Body 2/Semi-Bold */

  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  
  /* identical to box height, or 129% */

  /* Neutral/neutral black-500 */

  color: #1d1d1d;

  /* Inside auto layout */
`;

export const Input = styled.input`
  box-sizing: border-box;
  
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 10px 9px;
  gap: 10px;
  width:428px;
  
  min-height: 44px;

  outline:none;

  
  

  border: 1px solid #b9b9b9;
  border-radius: 8px;
  @media (max-width:535px) {
    max-width:300px;
  }
`;

export const ForgetPass = styled.button`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  border-style: none;
  /* identical to box height, or 129% */
  background-color: transparent;
  display: flex;
  justify-content: end;
  align-items: center;

  /* Blue/primary blue-600 */

  color: #1d2e88;
`;

export const SigninButton = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 16px;
  gap: 8px;
  border-style: none;
  max-width: 428px;
  min-height: 56px;
  background-color: transparent;
  color: #fff;
  /* Blue/primary blue-600 */

  background: #1d2e88;
  border-radius: 8px;
`;

export const NoAccount = styled.button`
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  background-color: transparent;
  /* identical to box height, or 133% */
  border-style: none;
  display: flex;
  align-items: center;

  /* Neutral/600 */

  color: #475569;
`;

export const FreeTrial = styled.button`
  font-family: "Inter";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  border-style: none;
  /* identical to box height, or 143% */
  background-color: transparent;

  /* Blue/primary blue-600 */

  color: #1d2e88;
`;

export const NoCredencial = styled.button`
  border-style: none;
  display: flex;
  justify-content: center;
  background-color: transparent;
`;


export const WelcomeSignupPage = styled.div`
  width: 45%;
  height: 100vh;
  background-color: #1d2e88;
  display:flex;
  flex-direction:column;
  justify-content:space-around;
  
  @media (max-width:848px) {
   display:none;
  }
`;


export const WelcomeTypo=styled.div`

font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 48px;
line-height: 60px;

/* identical to box height, or 125% */

display: flex;
align-items: center;

color: #FFFFFF;

`
export const WelcomeNormalTypo=styled.div`

font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;
/* or 150% */

display: flex;
align-items: center;

color: #FFFFFF;


`

export const WelcomeNormalContainer=styled.div`
display:flex;
padding-left:110px;
flex-direction:column;
justify-content:space-between;

`
export const WelcomeImage=styled.div`
padding-left:110px;

`

export const PassImgContainer=styled.div`

box-sizing: border-box;

  display: flex;
 /* align-items:center; */
  padding-right:10px;
  
  
  width:428px;
  
  min-height: 42px;

  outline:none;

  background: #ffffff;
  

  border: 1px solid #b9b9b9;
  border-radius: 8px;
  @media (max-width:535px) {
    max-width:300px;
  }

`

