import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  background-color: #f5f5f5;
height:1300px;
  flex-direction:column;
`;

export const Header = styled.div`
  background-color: #FFFFFF;;
  width: 100%;
  height: 80px;
  display:flex;
  align-items:center;
  justify-content:space-between;
  padding:20px 40px 20px 40px;
  
`;

export const DashboardContainer = styled.div`
  width: 100%;
  height: 100vh;
  /* background: blue; */
  display:flex;
`;

export const SideNavBar = styled.div`
  width: 220px;
  height:1140px;
   margin:20px;

/* Blue/primary blue-600 */

background-color: #1D2E88;
display: flex;
flex-direction: column;
justify-content: space-between;
padding: 20px ;
border-radius: 16px;
color: #FFFFFF;
`;

export const JobOpeningSection = styled.div`
 display:flex;
 flex-direction:column;
 width:100%;
`;



export const JobOpeningHeader = styled.div`
 display:flex;
 justify-content:space-between;
 height:70px;
 margin:60px 60px 40px;
`;

export const JobOpeningData= styled.div`
 display: flex;
flex-direction: column;
/* justify-content: center;
align-items: center; */
padding: 40px;

 height: 874px;
 margin:0px 56px 74px 40px;
background-color: #FFFFFF;
`;


export const Welcome= styled.div`
 font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 24px;

/* identical to box height, or 133% */


/* Neutral/neutral black-500 */

color: #1D1D1D;


/* Inside auto layout */

 
`;


export const Name= styled.div`
 width: 57px;
height: 20px;
line-height:2px;
/* Body 2/Regular */

font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 14px;
line-height: 20px;
/* identical to box height, or 143% */

display: flex;
align-items: center;

/* Neutral/neutral black-500 */

color: #1D1D1D;

 
`;


export const Line= styled.div`
 width: 0px;
height: 60px;

/* Neutral/neutral black-50 */

border: 1px solid #E8E8E8;


 
`;


export const SearchContainer= styled.div`
box-sizing: border-box;

/* Auto layout */

display: flex;

align-items: flex-start;
padding: 10px 9px;
gap: 10px;

width: 348px;
height: 44px;

/* Basic/white */

background: #FFFFFF;
/* Neutral/neutral black-100 */

border: 1px solid #B9B9B9;
border-radius: 8px;

/* Inside auto layout */

`;

export const SearchInput= styled.div`
width: 306px;
height: 24px;

/* Body 1/Regular */

font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;
/* identical to box height, or 150% */

display: flex;
align-items: center;

/* Neutral/neutral black-100 */

color: #B9B9B9;

 
`;


export const RightNavbarDiv= styled.div`
display:flex;
align-items:center;
width:350px;
justify-content:space-between;
`;


export const PlusButton= styled.div`

`;

export const MidNavbarDiv= styled.div`
display:flex;
justify-content:space-between;
gap:25px;
margin-right:190px;
@media (max-width:1178px) {
    margin-right:0px;

  }
 
`;


export const FaceNameContainer= styled.div`
display:flex;
align-items:center;
gap:5px;
`;

export const CollapseDiv= styled.div`
display:flex;
flex-direction:column;


`;



export const RecruitmentDiv= styled.div`
 display:flex;
flex-direction:column;
padding:10px;
gap:8px;
`;

export const OrganizationDiv= styled.div`
display:flex;
flex-direction:column;
gap:8px;
padding:10px;
`;

export const Finance= styled.div`
display:flex;
flex-direction:column;
gap:8px;
padding:10px;
`;


export const NavbarIconDataContainer= styled.button`
display: flex;
flex-direction: row;
align-items: center;
padding: 12px 20px 12px 12px;
gap: 8px;

width: 180px;
height: 44px;

/* Yellow/secondary yellow-600 */
background-color:transparent;
border-style:none;
border-radius: 8px;


`;

export const NavbarIcon= styled.div`
display:flex;
justify-content:center;
`;

export const NavbarData= styled.div`

/* Body 2/Regular */

font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 14px;
line-height: 20px;
/* identical to box height, or 143% */

display: flex;
align-items: center;

/* Basic/white */

color: #FFFFFF;

`;

export const Heading= styled.div`
width: 87px;


/* Caption 1/Semi-Bold */

font-family: 'Inter';
font-style: normal;
font-weight: 600;
font-size: 12px;

/* identical to box height, or 133% */

display: flex;
align-items: center;

/* Blue/primary blue-300 */

color: #8896E1;


/* Inside auto layout */

flex: none;
order: 0;
flex-grow: 0;
`;


export const Job= styled.div`
width: 77px;
height: 40px;

/* H3 */

font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 32px;
line-height: 40px;
/* identical to box height, or 125% */


/* Neutral/neutral black-500 */

color: #1D1D1D;
`;

export const CreateJob= styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 10px 16px;
gap: 8px;

color:#FFFF;
/* Blue/primary blue-600 */

background: #1D2E88;
border-radius: 8px;
`;

export const JobHeaderTwo=styled.div`
display:flex;
justify-content:space-between;
margin-top:30px;
`

export const MyOpenJobs=styled.div`
width: 168px;
height: 32px;

/* H4 */

font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 24px;
line-height: 32px;
/* identical to box height, or 133% */


/* Neutral/neutral black-500 */

color: #1D1D1D;



`


export const SearchConatiner=styled.div`

box-sizing: border-box;

/* Auto layout */

display: flex;

align-items:center;;
padding: 0px;
gap: 4px;
border: 2px solid #E8E8E8;;
width: 280px;
height: 44px;
border-radius:8px;
padding-left:5px;
img{
  width: 16.67px;
  height:16.67px;
}


`

export const SearchDiv=styled.div`

`
export const StatusJobContainer=styled.div`
display:flex;
gap:100px;
width:100%;

border-bottom:1px solid #E2E8F0;
@media (max-width:1584px) {
  border-bottom:none;
gap:0px;
justify-content:space-between;
}
`
export const TitleIconContainer=styled.button`
display:flex;
gap:3px;
background-color:transparent;
border-style:none;


`



export const JobStatusIcon=styled.div`
img{
  margin-bottom:5px;
}

`

export const JobStatusTitle=styled.button`

background:transparent;
height: 24px;
outline:none;
border:none;
font-family: 'Inter';
font-style: normal;
font-weight: 600;
font-size: 16px;
line-height: 24px;


color: #B9B9B9;

`

export const FilterButton=styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 10px 16px;
gap: 8px;

width: 95px;
height: 44px;
border-style:none;
/* Neutral/neutral black-50 */

background: #E8E8E8;
border-radius: 8px;


`

export const SearchFilterContainer=styled.div`
display:flex;
gap:20px;


`

export const SearchTitle=styled.div`

font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 24px;

/* identical to box height, or 150% */

display: flex;
align-items: center;

/* Neutral/neutral black-100 */

color: #B9B9B9;

`

export const FilterTitle=styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 600;
font-size: 14px;
line-height: 20px;
/* identical to box height, or 143% */


/* Neutral/neutral black-500 */

color: #1D1D1D;



`
export const Table=styled.table`

/* border: 2px solid red; */
width: 100%;
  /* height: 40px; */
/* margin-top:30px;
  border-collapse: collapse; */
  /* background-color: #f3f5ff; */
`

export const TableRow=styled.tr`


`

export const TableHeader=styled.td`
padding: 8px;
  text-align: center;
  border: none;
  background-color: #f3f5ff;

`


export const TableRowConatiner=styled.tr`
display:flex;
justify-content:space-between;

padding:8px 8px 8px 28px;
/* Blue/primary blue-50 */



`




export const TableData=styled.td`



padding: 8px;
  text-align: center;
  border: none;

`


export const Tr = styled.tr`
width:100%;
 
`;
export const Th = styled.th`
 
  padding: 8px;
  text-align: center;
  border: none;
  background-color: #f3f5ff;
`;
export const Td = styled.td`
 
  padding: 8px;
  text-align: center;
  border: none;
`;

export const EditDeleteContainer = styled.button`
 background-color:transparent;
  padding: 8px;
  text-align: center;
  border: none;
`;

export const Pagination=styled.div`



`