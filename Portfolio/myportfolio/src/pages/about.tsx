import React from 'react'
import Navbar from '../components/Navbar'
import Home from '../components/Home'
import Skills from '../components/Skills'
import Testimonial from '../components/Testimonial'
import Contact from '../components/Contact'

const About = () => {
  return (
    <>
    <div><Navbar/></div>
    <div><Home/></div>
    <div><Skills/></div>
    <div><Testimonial/></div>
    <div><Contact/></div>
    </>
  )
}

export default About