import React from 'react';
import logo from './logo.svg';
import './App.css';
import About from './pages/about';
function App() {
  return (
    <div className="App">
    <About/>
    </div>
  );
}

export default App;
