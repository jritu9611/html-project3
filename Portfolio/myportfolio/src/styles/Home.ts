import styled, { keyframes } from "styled-components";

export const MainHomeContainer = styled.div`
  min-width: 100%;
  height: 824px;

  display: flex;
`;
export const LeftHomeContainer = styled.div`
  width: 100%;
  display: flex;

  flex-direction: column;
  /* justify-content:center; */
  margin-left: 220px;

  text-align: left;
`;

export const LeftPartOne = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 80px;
  color: #212121;
  font-weight: 700;
  line-height: 2;
  /* padding-left:90px;
   padding-right:190px; */
`;
export const LeftPartTwo = styled.div`
  font-size: 17px;
  font-weight: 400;
  line-height: 1.7;
  font-family: sans-serif;
`;
export const LeftPartThree = styled.div`
  display: flex;
`;



export const LeftPartThreeRight = styled.button`
gap: 10px;
  justify-content: center;
  align-items: center;
  display: flex;
 
  height: 35px;
  color: #212121;
  font-weight:800;
  font-size:22px;
  border-style: none;
  outline: none;
  border-radius: 5px;
  margin-top: 15px;
  padding: 20px;
  margin-left:50px;
  background-color:#ffff;
`;




export const LeftPartThreeLeft = styled.button`
  gap: 10px;
  justify-content: center;
  align-items: center;
  display: flex;

  height: 35px;
  color: #ffff;
  background-color: lightsteelblue;
  border-style: none;
  outline: none;
  border-radius: 5px;
  margin-top: 15px;
  padding: 20px;
`;

export const LeftPartFour = styled.div`
margin-top:35px;
display:flex;`



export const LeftPartFourLeft = styled.div`
  display: flex;
  margin-top:15px;
  font-size:18px;
  font-family:400;
`;
export const LeftPartFourRight = styled.div`
padding-left:70px;
display:flex;

`

export const IconContainer=styled.div`
display:flex;
width:45px;
height:45px;
background-color:lightgrey;
border:1px solid black;
border-radius:50%;
margin-left:5px;
justify-content:center;
align-items:center;

`
export const RightHomeContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: Left;
  align-items:center;
`;

export const Icon = styled.div`
  animation: iconFly 1.5s infinite;

  @keyframes iconFly {
    
      0%,
      20%,
      50%,
      80%,
      100% {
        transform: translateY(0);
      }
      40% {
        transform: translateY(-30px);
      }
      60% {
        transform: translateY(-10px);
      }
    100% {
      transform: translateY(0);
    }
    40% {
      transform: translateY(-10px);
    }
    60% {
      transform: translateY(-5px);
    }
  };
`
export const Span=styled.div`
color:lightsteelblue;
line-height:45px;
`

