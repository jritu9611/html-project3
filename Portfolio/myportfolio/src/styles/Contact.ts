import styled from "styled-components";

export const ContactContainer = styled.div`
  height: 100vh;
  margin-top: 200px;
  display: flex;
  background-color: #c8aef9;
`;

export const ContactWrapper = styled.div`
  margin-left: 350px;
  padding-top: 60px;
`;
export const Heading = styled.h2`
  text-align: left;
  font-size: 60px;
  font-weight: 700;
  color: #ffffff;
`;

export const ContactDiv = styled.form`
  width: 800px;
  height: 600px;
  margin-top: -50px;
`;
export const FirstLine = styled.div`
  display: flex;
  gap: 60px;
  margin-top: -40px;
`;
export const First = styled.p`
  text-align: left;
`;
export const TextDiv = styled.p`
  text-align: left;
  margin-top: -22px;
`;
export const Header = styled.p`
  font-size: 25px;
  font-weight: 600;
  color: #ffffff;
`;
export const Input = styled.input`
  width: 280px;
  height: 40px;
  font-size: 20px;
  padding: 0 10px;
  border-radius: 8px;
  margin-top: -10px;
`;
export const Textarea = styled.textarea`
  border-radius: 8px;
  font-size: 20px;
  padding: 0 10px;
`;
export const Button = styled.button`
  width: 150px;
  height: 40px;
  margin-top: 600px;
  margin-left: 200px;
  font-size: 20px;
  border: none;
  color: #ffffff;
  background-color: #000000;
  border-radius: 10px;
  :hover {
    cursor: pointer;
    opacity: 0.7;
  }
`;