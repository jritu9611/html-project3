import styled from "styled-components";
export const NavbarContainer = styled.div`
padding-top:15px;
  max-width: 100%;
  
  display: flex;
  justify-content:center;
  /* background: linear-gradient(to right, lightsteelblue , lightgrey); */
`

export const NavIconAndName = styled.div`
  display: flex;
  flex-direction: column;
  margin-left:25px;
`;

export const NavIcon = styled.div`
/* padding-left:50px; */


`;

export const NavIconName = styled.div`
font-size:12px;
/* margin-right:20px; */
`;
