import styled from "styled-components";

export const TestimonialContainer = styled.div`
  width: 100%;

  min-height: 600px;
  background-color: lightgray;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Container = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 500px;
  height: 500px;
  border: 2px solid green;
`;
export const Testimonialdata = styled.div``;

export const TestimonialScroll = styled.div``;

export const TestimonialIcon = styled.div``;

export const Penback = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: 46px;
  height: 46px;
  background-color: #5c64cf;;
  animation: hithere 0.8s ease infinite;

  @keyframes hithere {
  40%, 60% { transform: rotate(-20deg); }
  50% { transform: rotate(20deg); }
  70% { transform: rotate(0deg) ; }
}
`



export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px;
  border-bottom: 2px solid #bcc6fc;
`


export const WebName = styled.div`
  font-size: 24px;
  font-weight: 700;
  color: white;
`

export const Form = styled.form`
  margin-top: 28px;
  padding: 0 24px;
`

export const Label = styled.label`
  font-size: 15px;
  font-weight: 600;
  line-height: 20px;
  padding-bottom:10px;
  color:white;
  width: 200px;
`


export const Input = styled.input`
  width: 608px;
  height: 24px;
  padding: 10px;
  border-radius: 12px;
  outline: none;
  border: 1px solid #b9b9b9;
  border-radius: 8px;
  font-size: 16px;
  margin-bottom: 20px;

  ::placeholder {
    color: #b9b9b9;
  }
`


export const Textarea = styled.textarea`
  width: 608px;
  height: 100px;
  border: 1px solid #b9b9b9;
  border-radius: 8px;
  font-size: 16px;
  outline: none;
  padding: 10px;
  margin-top: 4px;
  margin-bottom: 36px;

  ::placeholder {
    color: #b9b9b9;
    line-height: 24px;
  }
`

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`


export const InputContainer = styled.div`
  display: flex;
  gap: 20px;
`

export const TextareaWrapper = styled.div`
  /* margin-top: 28px; */
  /* margin-bottom:36px; */
`

export const ButtonWrapper = styled.div`
  display: flex;
  gap: 8px;
  /* float: right; */
  padding: 20px;
  height: 44px;
  
`;
export const Button = styled.button`
  font-size: 16px;
  border-radius: 8px;
  cursor: pointer;
  background-color: #e8e8e8;
  border: none;
  padding: 10px 44px;
  text-align: center;
  font-weight: 600;
  box-shadow: 0 0 20px #6600cc;
`;

export const CloseBtn = styled.div`
    font-size: 25px;
    cursor: pointer;
`;