import styled from "styled-components";

export const SkillContainer = styled.div`
  display: flex;
  justify-content: center;
  height: 400px;
  width: 100%;
  border: 1px solid black;
`;

export const SkillIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top:50px;
  padding: 10px;
  margin-left: 10px;
  width: 45px;
  height: 45px;
  border: 3px solid lightgray;

  border-radius: 50%;
  `
export const SKills=styled.div`
margin-top:50px;
font-size:60px;
`
  export const Icon=styled.div`
  animation: iconFly 1.5s infinite;
   @keyframes iconFly {
  
    0%,
    20%,
    50%,
    80%,
    100% {
      transform: translateY(0);
    }
    40% {
      transform: translateY(-30px);
    }
    60% {
      transform: translateY(-10px);
    }
  100% {
    transform: translateY(0);
  }
  40% {
    transform: translateY(-10px);
  }
  60% {
    transform: translateY(-5px);
  }
};
`;
