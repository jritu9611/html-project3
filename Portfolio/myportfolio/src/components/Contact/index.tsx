import React from "react";
import * as fileSaver from "file-saver";
import { useForm } from "react-hook-form";
import { FiSend } from "react-icons/fi";
import Modal from "../../components/Home"
// import { IContactData } from "interfaces/IContactData";
// import { TalkDiv } from "styles/components/Dashboard";

import {
  ContactContainer,
  ContactWrapper,
  Heading,
  ContactDiv,
  FirstLine,
  First,
  Header,
  Input,
  TextDiv,
  Textarea,
  Button,
} from "../../styles/Contact";
const saveFile = () => {
  fileSaver.saveAs(
    process.env.PUBLIC_URL + "/resources/Ritu's CV.pdf",
    "Ritu's CV.pdf"
  );
};

const Contact = () => {
  const { register } = useForm<any>();
  return (
    <ContactContainer>
      <ContactWrapper>
        <Heading>Let's Talk</Heading>
        <ContactDiv>
          <FirstLine>
            <First>
              <Header>First Name</Header>
              <Input
                {...register("firstname")}
                type="text"
                placeholder="Your FirstName"
              />
            </First>
            <First>
              <Header>Last Name</Header>
              <Input
                {...register("lastname")}
                type="text"
                placeholder="Your LastName"
              />
            </First>
          </FirstLine>
          <FirstLine>
            <First>
              <Header>Email</Header>
              <Input
                {...register("email")}
                type="email"
                placeholder="Your Email"
              />
            </First>
            <First>
              <Header>Subject</Header>
              <Input
                {...register("subject")}
                type="text"
                placeholder="Your Subject"
              />
            </First>
          </FirstLine>
          <TextDiv>
            <Header>Message</Header>
            <Textarea
              cols={53}
              rows={10}
              placeholder="Your Message 😄"
            ></Textarea>
          </TextDiv>

          <First>
            <button>
              Send
              <FiSend className="send" />
            </button>
          </First>
        </ContactDiv>
      </ContactWrapper>

      <Button className="cv" onClick={saveFile}>
        Download CV
      </Button>
    </ContactContainer>
  );
};

export default Contact;
