import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { IoMdContacts } from "react-icons/io";
import Modal from "react-modal";
import Slider from "react-slick";
import { FaPenAlt } from "react-icons/fa";
import {
  TestimonialContainer,
  Container,
  Testimonialdata,
  TestimonialScroll,
  TestimonialIcon,
  Penback,
  Header,
  WebName,
  Form,
  InputContainer,
  InputWrapper,
  Label,
  CloseBtn,
  TextareaWrapper,
  Textarea,
  Input,
  ButtonWrapper,
  Button,
} from "../../styles/Testimonial";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "rgb(75 83 190)",
  },
};

Modal.setAppElement("#root");
const Testimonial = () => {
  let subtitle: any;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
  }

  function closeModal() {
    setIsOpen(false);
  }

  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div>
      <TestimonialContainer>
        <Container>
          <Testimonialdata>Tetimonial</Testimonialdata>
          <TestimonialScroll>
            <Slider {...settings} className="slider">
              {/* <div style={{display:"flex"}}> */}
              <div>
                <h3>1</h3>
              </div>
              <div>
                <h3>2</h3>
              </div>
              <div>
                <h3>3</h3>
              </div>
              <div>
                <h3>4</h3>
              </div>
              <div>
                <h3>5</h3>
              </div>
              <div>
                <h3>6</h3>
              </div>
              {/* </div> */}
            </Slider>
          </TestimonialScroll>
          <Penback onClick={openModal}>
            <FaPenAlt style={{ color: "#fff" }} size={24} />
          </Penback>

          <Modal
            isOpen={modalIsOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Example Modal"
          >
            <Header>
              <WebName>Your Testimonial</WebName>
              <CloseBtn onClick={closeModal}></CloseBtn>
            </Header>

            <Form>
              <InputContainer>
                <InputWrapper>
                  <Label>
                    Your Name<sup>*</sup>
                  </Label>
                  <Input
                    // {...register("title")}
                    placeholder="Enter Title"
                    name="title"
                    // onChange={(e) => onValueChange(e)}
                  />
                </InputWrapper>
                <InputWrapper></InputWrapper>
              </InputContainer>
              <InputWrapper>
                <Label>
                  Message<sup>*</sup>
                </Label>
                <TextareaWrapper>
                  <Textarea
                    // {...register("decreption")}
                    placeholder="Enter the Job Description"
                    name="decreption"
                    // onChange={(e) => onValueChange(e)}
                  />
                </TextareaWrapper>
              </InputWrapper>
            </Form>
            <ButtonWrapper>
              <Button onClick={openModal}>Cancel</Button>
              <Button
                style={{ backgroundColor: "#1D2E88", color: "white" }}
                // onClick={onHandleSubmit}
              >
                Submit
              </Button>
            </ButtonWrapper>
          </Modal>
          <TestimonialIcon>kgjrjy</TestimonialIcon>
        </Container>
      </TestimonialContainer>
    </div>
  );
};

export default Testimonial;
