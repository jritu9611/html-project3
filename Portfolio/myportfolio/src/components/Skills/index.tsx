import React from "react";
import { SkillContainer, SkillIcon } from "../../styles/Skills";
import { FaReact } from "react-icons/fa";
import { FaNodeJs } from "react-icons/fa";
import { SiExpress } from "react-icons/si";
import { FaVuejs } from "react-icons/fa";
import { GrHtml5 } from "react-icons/gr";
import { BsFiletypeCss } from "react-icons/bs";
import { Icon,SKills} from "../../styles/Skills";
const Skills = () => {
  return (
    <div>
      <SkillContainer>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div>
            <SKills>Skills</SKills>
          </div>
          <div style={{display:"flex"}}>
            <SkillIcon>
              <Icon>
                <FaReact size={50} />
              </Icon>
            </SkillIcon>
            <SkillIcon>
              <Icon>
                <FaNodeJs size={50} />
              </Icon>
            </SkillIcon>
            <SkillIcon>
              <Icon>
                <SiExpress size={50} />
              </Icon>
            </SkillIcon>
            <SkillIcon>
              <Icon>
                <GrHtml5 size={50} />
              </Icon>
            </SkillIcon>
            <SkillIcon>
              <Icon>
                <FaVuejs size={50} />
              </Icon>
            </SkillIcon>
            <SkillIcon>
              <Icon>
                <BsFiletypeCss size={50} />
              </Icon>
            </SkillIcon>
          </div>
        </div>
      </SkillContainer>
    </div>
  );
};

export default Skills;
