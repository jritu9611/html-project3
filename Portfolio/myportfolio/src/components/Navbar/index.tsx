import React from "react";
import { IoMdHome } from "react-icons/io";
import { GrCertificate } from "react-icons/gr";
import { ImWoman } from "react-icons/im";
import { GrProjects } from "react-icons/gr";
import { MdOutlineContactSupport } from "react-icons/md";
import { MdDarkMode } from "react-icons/md";

import {
  NavbarContainer,
  NavIconAndName,
  NavIcon,
  NavIconName,
} from "../../styles/Navbar";

const Navbar = () => {
  return (
    <>
      <NavbarContainer>
        <NavIconAndName>
          <NavIcon>
            <IoMdHome />
          </NavIcon>
          <NavIconName>Home</NavIconName>
        </NavIconAndName>
        <NavIconAndName>
          <NavIcon><GrCertificate /></NavIcon>
          <NavIconName>Certification</NavIconName>
        </NavIconAndName>
        <NavIconAndName>
          <NavIcon><ImWoman /></NavIcon>
          <NavIconName>About</NavIconName>
        </NavIconAndName>
        <NavIconAndName>
          <NavIcon><GrProjects /></NavIcon>
          <NavIconName>Projects</NavIconName>
        </NavIconAndName>
        <NavIconAndName>
          <NavIcon><MdOutlineContactSupport /></NavIcon>
          <NavIconName>Contact</NavIconName>
        </NavIconAndName>
        <NavIconAndName>
          <NavIcon><MdDarkMode /></NavIcon>
          <NavIconName>Light</NavIconName>
        </NavIconAndName>
      </NavbarContainer>
    </>
  );
};

export default Navbar;
