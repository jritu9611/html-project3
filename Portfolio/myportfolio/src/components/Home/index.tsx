import Typewriter from "typewriter-effect";
import { FaLocationArrow } from "react-icons/fa";
import { BsGithub } from "react-icons/bs";
import { AiFillLinkedin } from "react-icons/ai";
import { AiFillInstagram } from "react-icons/ai";
import { BsArrowUpRightCircle } from "react-icons/bs";

import {
  MainHomeContainer,
  LeftHomeContainer,
  RightHomeContainer,
  LeftPartOne,
  LeftPartTwo,
  LeftPartThree,
  LeftPartFour,
  LeftPartThreeLeft,
  LeftPartThreeRight,
  LeftPartFourLeft,
  LeftPartFourRight,
  Icon,
  IconContainer,
  Span
} from "../../styles/Home";

import React from "react";
import myphoto from "../../assets/Images/myphoto.jpg";
const Home = () => {
  return (
    <>
      <MainHomeContainer>
        <LeftHomeContainer>
          <LeftPartOne>
            <div >I Have </div>
            <div>
              <Span>
              <Typewriter 
                options={{
                  autoStart: true,
                  loop: true,    
                  delay: 40,
                  strings: [
                    "Web Develo",
                    "Frontend ",
                    "UI Developer",
                  ],
          
                }}
              />
              </Span>
            </div>
            <div >Experience</div>
          </LeftPartOne>
          <LeftPartTwo>
            Hi, I'm Ayush, Full-Stack Web Developer - creating bold <br />&
            brave interface design and web application for <br /> companies all
            across the world.
          </LeftPartTwo>
          <LeftPartThree>
          <LeftPartThreeLeft>
            Lets Talk
            <Icon><FaLocationArrow /></Icon>
           
         
          </LeftPartThreeLeft>
          <LeftPartThreeRight>Projects<BsArrowUpRightCircle fontWeight={"800"} color="#000000"/></LeftPartThreeRight>
          </LeftPartThree>
          <LeftPartFour>
          <LeftPartFourLeft>check out my</LeftPartFourLeft>
          <LeftPartFourRight>
            <IconContainer>
              <AiFillLinkedin size={30}/>
            </IconContainer>
            <IconContainer>
              <AiFillInstagram size={30}/>
            </IconContainer>
            <IconContainer>
              <BsGithub size={30}/>
            </IconContainer>
          </LeftPartFourRight>
          </LeftPartFour>
        </LeftHomeContainer>

        <RightHomeContainer>
          <img
            src={myphoto}
            alt="Ritu"
            style={{ height: "500px", width: "500px" }}
          ></img>
        </RightHomeContainer>
      </MainHomeContainer>
    </>
  );
};

export default Home;
