// Create 4 json files, utilize all 4 promise methods namely:
// Promise.all, promise.allSettled, promise.any, promise.race
// to read these files in parallel.





let fs=require("fs").promises

const asyncFun1=async()=>{
const data = await fs.readFile("p1.json","utf8");
const data2=JSON.parse(data)
return data2
}

const asyncFun2=async()=>{
    const data = await fs.readFile("p1.json","utf8");
    const data2=JSON.parse(data)
    return data2
    }

    const asyncFun3=async()=>{
        const data = await fs.readFile("p1.json","utf8");
        const data2=JSON.parse(data)
        return data2
    }

    const asyncFun4=async()=>{
            const data = await fs.readFile("p1.json","utf8");
            const data2=JSON.parse(data)
            return data2
    }

    Promise.all([asyncFun1(),asyncFun2(),asyncFun3(),asyncFun4()]).then((data)=>{
    console.log(data)
    })
         
    


            
