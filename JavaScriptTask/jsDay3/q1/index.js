// Create a json file which contains array of objects ( person ), e.g. 
// [ { firstName: ‘’, lastName: ‘’, age: ‘’, address: ‘’ }, { firstName: ‘’, lastName: ‘’, age: ‘’, address: ‘’ } ]
// Read this file both synchronously and asynchronously using the ‘fs’ module 
// parse its data and filter only the objects having age greater than 25
// transform ‘firstName’ and ‘lastName’ to capitalize
// update the file with this transformed data.
// Do this using all three ways: callbacks, promises and async await 






const fs = require("fs");


//Asynchronous Method

let asyncdata = fs.readFile("person.json", "utf8", function (err, data) {
  //console.log(data);
  return data;
});
//Synchronous Method
const promise1 = () => {
  let syncdata = fs.readFileSync("person.json", "utf8", function (err, data) {
    return data;
  });
//convet json data to object
  return JSON.parse(syncdata);
};
let data = promise1();
let filterobj = data.filter((obj) => {
  if (obj.age > 25) {
    obj.firstName = obj.firstName.toUpperCase();
    obj.lastName = obj.lastName.toUpperCase();
    return obj;
  }
});
console.log(filterobj);
//convert object to json
fs.writeFileSync("./copy.json", JSON.stringify(filterobj));
