let fs = require("fs");

const myPromise = new Promise((resolve, reject) => {
  fs.readFile("person.json", (error,obj) => {
    if (error) {
      reject("error occured");
    } else {
      resolve(JSON.parse(obj));
    }
  });
});
myPromise
  .then((obj) => {
    element = obj.filter((element) => {
      if (element.age > 25) {
        element.firstName = element.firstName.toUpperCase();
        element.lastName = element.lastName.toUpperCase();
        return element;
      }
    });
    return element;
  })
  .then((obj) => {
    fs.writeFile("promise.json", JSON.stringify(obj), (err) => {
      console.log("Data Send");
    });
  })
  .catch((err) => {
    console.log("err");
  });

