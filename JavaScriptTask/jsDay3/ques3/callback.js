const fs = require("fs");

function fun1(callback) {
  setTimeout(() => {
    callback(2);
  }, 2000);
}
function fun2(callback) {
  fun1((result1) => {
    setTimeout(() => {
      callback(result1 + 3);
    }, 2000);
  });
}
function fun3(callback) {
  fun2((result2) => {
    setTimeout(() => {
      callback(result2 + 4);
    }, 2000);
  });
}
function fun4(callback) {
  fun3((result3) => {
    setTimeout(() => {
      callback(result3 + 5);
    }, 2000);
  });
}
function callback(value) {
  console.log(value);
}
fun4(callback);
