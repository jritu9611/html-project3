function revWord(str) {
    let rev = "";
   
    for(let i=str.length-1; i>=0; i--){
        rev+= str[i];
    }
    return rev;
}


//Step 1 -> reverse full string
let str = "Hello world";
let reverseFull = revWord(str);

let wordArr = reverseFull.split(' ');


wordArr.reverse();
let ans = wordArr.join(' ');
console.log(ans);