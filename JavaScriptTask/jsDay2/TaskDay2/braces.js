// // function braces(s) {
// //   let stack = [];
  
// //   for (let i = 0; i < s.length; i++){
// //     let top=stack[stack.length-1]
// //     if (s[i] ==="[" || s[i] === "(" || s[i] === "{") {
// //       stack.push(s[i]);
     
// //     }
// //     else if(s[i]==="}" && s[top]==="{" && stack.length!==0){
// //    stack.pop();
// //     }
// //     else if(s[i]===")" && s[top]==="(" && stack.length!==0){
// //         stack.pop();
// //          }
// //          else {
// //            break
// //          }
// // }
// // return stack.length===0
// // }


// console.log(braces("[]"))


// Given a string possibly containing three types of braces ({}, [], ()), write a function that returns a Boolean indicating whether the given string contains a valid nesting of braces.

function validBraces(braces){
    let tracer = []
    for(let i=0;i < braces.length; i++){
      if ( braces[i] === "(" || braces[i] === "{" || braces[i] === "["){
        tracer.push(braces[i])
      } else{
        if(tracer.length === 0) return false
        let lastValue = tracer[tracer.length-1]
        if( (braces[i] === ']' && lastValue === '[') || (braces[i] === '}' && lastValue === '{') || (braces[i] === ')' && lastValue === '('))
        {
          tracer.pop()
        } else {
          break;
        }
      }
    }
    return tracer.length === 0
  }
  
  
  console.log(validBraces( "(" )) 
  console.log(validBraces( "[]" )) 
  console.log(validBraces( "{}" ))
  console.log(validBraces( "(){}[]")) 