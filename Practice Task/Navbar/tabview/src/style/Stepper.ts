import styled from "styled-components";

export const OrderStepperWrapper = styled.div`
  width: 400px;
  height: auto;
  display: flex;
  /* border-top: 1px solid rgba(0, 0, 0, 0.1); */
  /* padding-top: 8px; */
  /* border-bottom: 1px solid rgba(0, 0, 0, 0.1); */
  padding-bottom: 20px;

  .ant-dropdown-menu {
    border: 5px solid green;
  }

  .ant-steps.ant-steps-vertical > .ant-steps-item {
    margin: 11px 0;
  }

  .ant-steps
    .ant-steps-item-finish
    .ant-steps-item-icon
    > .ant-steps-icon
    .ant-steps-icon-dot {
    background: #419e6a;
  }

  .ant-steps .ant-steps-item-process .ant-steps-item-icon {
    background-color: #419e6a;
    color: white;
    border-color: unset;
  }

  .ant-steps .ant-steps-item-finish .ant-steps-item-icon {
    background-color: #419e6a;
    color: white;
    border-color: unset;
  }
  .ant-steps .ant-steps-item-finish .ant-steps-item-icon > .ant-steps-icon {
    color: white;
  }
  .ant-steps
    .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-tail::after {
    background-color: #419e6a;
  }
  .ant-steps.ant-steps-vertical
    > .ant-steps-item
    > .ant-steps-item-container
    > .ant-steps-item-tail::after {
    width: 4px;
  }
  .ant-steps-item-subtitle {
    font-family: "Inter";
    font-weight: 500;
    font-size: 12px;
    line-height: 15px;
    color: #8a8a8a;
  }
  .ant-steps-item-title {
    font-family: "Inter";
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #1d1d1d;
  }
  .ant-steps .ant-steps-item-description {
    font-family: "Inter";
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
  }
  .ant-steps
    .ant-steps-item-wait
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    font-family: "Inter";
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #1d1d1d;
  }
  .ant-steps
    .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description {
    color: #979797;
  }
  .ant-steps .ant-steps-item-title {
    padding-inline-end: 0px;
  }
  .ant-steps.ant-steps-vertical > .ant-steps-item .ant-steps-item-icon {
    margin-inline-end: 2px;
  }
`;
