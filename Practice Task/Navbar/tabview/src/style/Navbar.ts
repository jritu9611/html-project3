import styled from "styled-components";

export const MainContainer = styled.div`
  .ant-tabs-tab {
    border: 2px solid #91caff;
    padding: 5px 20px;
    border-radius: 15px;
    color: #fff;
    font-size: 15px;
    font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS",
      sans-serif;
    /* background-color:#91caff;; */
  }
  .ant-tabs-nav-wrap {
    display: flex;
    justify-content: center;
    border: 2px solid #fff;
    background-color: #91caff69;
    /* color:#91caff; */
    font-family: serif;
    font-weight: 600;
    outline: none;
    border-radius: 10px;
  }
  .ant-tabs-tab:hover {
    color: blue;
    border: 2px solid #91caff69;

    background-color: lightcoral;
  }

  .ant-tabs-ink-bar {
    background-color: transparent;
  }
`;
