import styled from "styled-components";

export const MainContainer=styled.div`
display:flex;
justify-content:center;
align-items:center;
position:relative;
`



export const ConnectingLine=styled.div`
height:200px;
width:1px;
border-left:2px dashed black;
color:black;
`

export const ConnectingCircle=styled.div`
height:5px;
width:5px;
border:2px dashed black;
background-color:black;
border-radius:50%;
color:black;
position:absolute;
`

