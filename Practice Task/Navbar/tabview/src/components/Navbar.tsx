import React from 'react';
import { Tabs } from 'antd';
import type { TabsProps } from 'antd';
import { MainContainer } from '../style/Navbar';
const onChange = (key: string) => {
  console.log(key);
};

const items: TabsProps['items'] = [
  {
    key: '1',
    label: `All`,
   
  },
  {
    key: '2',
    label: `Confirmed`,
   
  },
  {
    key: '3',
    label: `Assigned`,
   
  },
  {
    key: '4',
    label: `On Delivery`,
   
  },
]

const App: React.FC = () => 
<MainContainer>
<Tabs defaultActiveKey="1" items={items} onChange={onChange} />
</MainContainer>

export default App;