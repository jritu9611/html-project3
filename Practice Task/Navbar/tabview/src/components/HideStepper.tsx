import React, { useState } from "react";
import { Steps } from "antd";
import { OrderStepperWrapper } from "../style/Stepper";
import { MdKeyboardArrowDown } from "react-icons/md";
import Dropdown from "../components/Drobdown";
import { DropdownContainer } from "../style/Dropdown";
// import { MdKeyboardArrowDown } from 'react-icons/md';

// MdKeyboardArrowDown
const { Step } = Steps;

const HideStepper = () => {
  const [currentStep, setCurrentStep] = useState(0);
  const [pathStep, setPathStep] = useState<boolean>(false);

  const handleClick = (step: number) => {
    setCurrentStep(step);
    if (step === 1) {
      setPathStep(true);
    }
  };

  const ShowDropdown = () => {
    console.log();
  };

  const stepContent = [
    {
      step: 0,
      title: "Package Ordered",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: " ",
   
    },
    {
      step: 1,
      title: "Shipped",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "package arrived at lorem sipum station",
     
    },
    {
      step: 2,
      title: "Out for delivery",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "",
    
    },
    {
      step: 4,
      title: "Out for delivery",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "",
   
    },
    {
      step: 3,
      title: "Arriving On Wednesday",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "",
    
    },
  ];

  return (
    <OrderStepperWrapper>
      <Steps direction="vertical" current={currentStep}>
        {stepContent.map((stepObj) => (
          <Step
            key={stepObj.step}
            title={stepObj.title}
            subTitle={stepObj.subTitle}
            description={stepObj.description}
        
            onClick={() => handleClick(stepObj.step)}
          />
        ))}
      </Steps>
      <Steps direction="vertical" current={currentStep}>
        <>
          {/* {
          stepContent.map((stepObj)=>{
            console.log(stepObj.step)
            if(stepObj.step ==3){
              console.log(stepObj.step == 3);
             return  <Dropdown/>
            }
          })
          
        } */}
        </>
      </Steps>
      <DropdownContainer>{pathStep && <Dropdown />}</DropdownContainer>
    </OrderStepperWrapper>
  );
};

export default HideStepper;
