import React, { useState } from "react";
import { Steps } from "antd";
import { MdKeyboardArrowDown } from "react-icons/md";
import { MdKeyboardArrowUp } from 'react-icons/md';
import { OrderStepperWrapper } from "../style/Stepper";
import { DropdownContainer } from "../style/Dropdown";
import HideStepper from "./HideStepper";
const { Step } = Steps;

const OrderStepper = () => {
  const [currentStep, setCurrentStep] = useState(0);
  const [dropdownValue, setDropdownValue] = useState("");
  const [pathStep, setPathStep] = useState<boolean>(false);
  const [showNestedStepper, setShowNestedStepper] = useState(false);
  const handleClick = (step: any) => {
    setCurrentStep(step);
    setDropdownValue(step);
    if (step === 1) {
      setPathStep(true);
      
    }
  };

  const handleNext = () => {
 
    setShowNestedStepper(!showNestedStepper);
    // setCurrentStep(currentStep + 1)
  };

  const stepContent = [
    {
      step: 0,
      title: "Package Ordered",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: " ",
    },
    {
      step: 1,
      title: showNestedStepper?<HideStepper/>:"",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "package arrived at lorem sipum station",
      
    },
    {
      step: 2,
      title: "Out for delivery",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "",
    },
    {
      step: 3,
      title: "Arriving On Wednesday",
      subTitle: "Saturday 30/04/23 at 4:30pm",
      description: "",
    },
  ];

  return (
    <OrderStepperWrapper>
      <Steps direction="vertical" current={currentStep}>
        {stepContent.map((stepObj) => (
          <Step
            key={stepObj.step}
            title={stepObj.title}
            subTitle={stepObj.subTitle}
            description={stepObj.description}
            onClick={() => 
                
              handleClick(stepObj.step)}
          ></Step>
          
        ))}
      </Steps>
      <DropdownContainer>
        <button onClick={handleNext} style={{position:"absolute",top:"80px",backgroundColor:"transparent",border:"none"}}>
          {/* {pathStep && <MdKeyboardArrowDown size={25}/>} */}
          {pathStep && <MdKeyboardArrowDown size={25}/>}
        </button>
      </DropdownContainer>
    </OrderStepperWrapper>
  );
};

export default OrderStepper;

