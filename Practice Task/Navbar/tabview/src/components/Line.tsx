import React from 'react'
import { ConnectingLine,MainContainer,ConnectingCircle } from '../style/Line'


const Line = () => {
  return (
    <div>
        <MainContainer>
        <ConnectingLine></ConnectingLine>
        <ConnectingCircle></ConnectingCircle>
        </MainContainer>
    </div>
  )
}

export default Line