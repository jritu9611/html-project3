import React from "react";
import ReactDOM from "react-dom";
import "antd";
import "./index.css";
import { Steps, Menu, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
const { Step } = Steps;

const menu = (
  <Menu>
    <Menu.Item>A</Menu.Item>
    <Menu.Item>B</Menu.Item>
    <Menu.Item>C</Menu.Item>  
  </Menu>
);

export const CustomStepper = () => {
  return (
    <Dropdown overlay={menu}>
      <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
        Hover me <DownOutlined />
      </a>
    </Dropdown>
  );
};

const App = () => {
  return (
    <Steps current={1}>
      <Step title={<CustomStepper />} description="This is a description." />
      <Step title="In Progress" description="This is a description." />
      <Step title="Waiting" description="This is a description." />
    </Steps>
  );
};

ReactDOM.render(<App />, document.getElementById("container"));
