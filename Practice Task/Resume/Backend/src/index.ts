import express from "express";


const fileUpload = require("express-fileupload");
const pdfparse = require("pdf-parse");
const cors = require("cors");
const app = express();


app.use(express.json());
app.use(cors());
app.use(fileUpload());

app.use("/", express.static("public"));

app.get("/", (req: any, res: any) => {
  // console.log(req.file)

  res.send(200);
});

app.post("/extract-text", (req: any, res: any) => {
  if (!req.files && !req.files.pdfFile) {
    res.status(400);
    res.end();
  }
 
  pdfparse(req.files.pdfFile)
  .then((result: any) => {
    console.log(result.text)
    res.send(result.text);
  });
});

app.listen(4000, () => {
  console.log("server started");
});
