import React from "react";

import useCopyToClipboard from "../component/useCutom";

function CopyButton( {code}:{code:string}):any{
  const [isCopied, handleCopy] = useCopyToClipboard();

  return (
    <button onClick={() => handleCopy(code)}>
      {isCopied ? "j" :"hg"}
    </button>
  );
}