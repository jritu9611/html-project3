import React from "react";
import { useForm } from "react-hook-form";
function Tool() {
  const { register, handleSubmit } = useForm();
  const Input = ({ label, register, required }: any) => {
    return (
      <>
        <label>{label}</label>
        <input {...register(label, { required })} />
      </>
    );
  };

  const Select = (
    ({ onChange, onBlur, label,name }: any) => {
      return (
        <>
        <br></br>
          <label>{label}</label>
          <select>
            <option value="20">20</option>
            <option value="30">30</option>
          </select>
        </>
      );
    }
  );

  const onSubmit = (data:any) => {
    alert(JSON.stringify(data));
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input label="name" register={register}></Input>
        <input type="submit" />
        <Select label="age" {...register("age")}></Select>
      </form>
    </>
  );
}

export default Tool;






// import React from "react";
// import { useForm } from "react-hook-form";
// function Tool() {
//   const { register, handleSubmit } = useForm();
//   const Input = ({ label, register, required }: any) => {
//     return (
//       <>
//         <label>{label}</label>
//         <input {...register(label, { required })} />
//       </>
//     );
//   };

//   const Select = (
//     ({ onChange, onBlur, label,name }: any) => {
//       return (
//         <>
//         <br></br>
//           <label>{label}</label>
//           <select>
//             <option value="20">20</option>
//             <option value="30">30</option>
//           </select>
//         </>
//       );
//     }
//   );

//   const onSubmit = (data:any) => {
//     alert(JSON.stringify(data));
//   };
//   return (
//     <>
//       <form onSubmit={handleSubmit(onSubmit)}>
//         <Input label="name" register={register}></Input>
//         <input type="submit" />
//         <Select label="age" {...register("age")}></Select>
//       </form>
//     </>
//   );
// }

// export default Tool;









// import React from "react";
// import { useForm } from "react-hook-form";
// function Tool() {
//   const {
//     register,
//     handleSubmit,
//     watch,
//     formState: { errors },
//   } = useForm();

//   const onSubmit = (data: any) => {
//     console.log(data);
//   };
//   return (
//     <>
//       <form onSubmit={handleSubmit(onSubmit)}>
//         <label>Name</label>
//         <input
//           {...register("userName",{required:true, maxLength: 20 }
//             //{ onBlur: (e) => console.log(e.target.value) }
//           )}
//         />
//         {errors.userName?.type==="required" && <span>Username is required</span>}
//         {errors.userName?.type==="maxLength" && <span>length cannot be more than 20 word</span>}
//         <button>Submit</button>
//       </form>
//     </>
//   );
// }

// export default Tool;
