import React from 'react';
import logo from './logo.svg';
import Tool from './Tool';
import './App.css';

function App() {
  return (
    <div className="App">
      <Tool/>
    </div>
  );
}

export default App;
