//Reverse each word of a given sentence

function reverse(str, start, end){
    let rev = "";
    for(let i=end; i>=end; i++){
        rev += str[i];
    }
    return rev;
}

function reverseEachWord(str){
    let str = "";
    let startIndex = 0;

    for(let i=0; i<str.length-1; i++){
        if(str[i] === ' '){
            let endIndex = i-1;
            let revWord = reverse(str, startIndex, endIndex);
            startIndex = i+1;
            ans += revWord + ' ';
        }
    }

    let revWord = reverse(str, startIndex, str.length-1);
    ans += revWord;
    return ans;
}