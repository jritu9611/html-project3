import React from 'react'
import { useRouter } from 'next/router'
const pageno = () => {
    const router=useRouter()
    const PageNumber=router.query.pageno
  return (
    <div>{PageNumber}</div>
  )
}

export default pageno