const express = require("express");
const kafka=require("kafka-node")
const mongoose=require('mongoose')
const app = express();

app.use(express.json());

const dbsAreRunning = () => {
  mongoose.connect(process.env.model.MONGO_URL)
  const User=new mongoose.model('user',{
    name:String,
    email:String,
    password:String
  })
  };
  setTimeout(dbsAreRunning, 10000);
  app.listen(process.env.PORT);

