import React from 'react';
import logo from './logo.svg';
import { createContext } from 'react';

import {ComA} from './context/comA';

import './App.css';


export const FirstName=createContext("")


function App() {
  return (
   
      <>
      <FirstName.Provider value={"tasks"}>
      <ComA/>
      </FirstName.Provider>
      </>
   
  );
}

export default App;
