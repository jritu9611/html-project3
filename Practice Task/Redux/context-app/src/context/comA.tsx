import React from 'react'
import { ComB } from './comB'
import { FirstName } from '../App'
export const ComA = () => {
  return (
    <FirstName.Consumer>
    {theme => (
      <button className={theme} />
    )}
  </FirstName.Consumer>
  )
}
