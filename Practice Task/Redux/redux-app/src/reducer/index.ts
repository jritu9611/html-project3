import { createSlice } from "@reduxjs/toolkit";

 const counterSlice=createSlice({
    name:"slice",
    initialState:{counter:0},
    reducers:{
        increment(state,action):any{
            
            state.counter+=1;
            
          
        // state.counter++;
        },
        decrement(state,action){
            state.counter--;
        },
        add(state,action){
            state.counter+=action.payload
        },
    }
})

export const action=counterSlice.actions

export const reducerFns=counterSlice.reducer