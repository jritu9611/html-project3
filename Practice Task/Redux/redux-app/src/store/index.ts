import { configureStore } from "@reduxjs/toolkit";
import { persistReducer,persistStore } from "redux-persist";
import { combineReducers } from "@reduxjs/toolkit";


import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
 
import { reducerFns } from "../reducer";


const persisteConfig={
  key:"root-persist",
  storage
}

const reducer=combineReducers({reducerFns})
//it store the reducerfns on the localstorage
const persistedReducer=persistReducer(persisteConfig,reducer)

const store=configureStore({
    reducer:persistedReducer,
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
})


const persistor=persistStore(store)


export default store;
export {persistor}