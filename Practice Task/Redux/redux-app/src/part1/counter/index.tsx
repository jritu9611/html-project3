
import React from 'react'
import { useSelector,useDispatch} from 'react-redux'
function Counter() {
  const counter=useSelector((state:any)=>{
    return state.counter
  
})

  const dispatch=useDispatch()
  const increment=()=>{
    
    dispatch({type:"INC"})
  }
  const decrement=()=>{
    dispatch({type:"DEC"})
  }
  const add=()=>{
    dispatch({type:"Add",payload:10})
  }
   
   
  return (
    <div>
      <h1>Counter app</h1>
      <h2>{counter}</h2>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <button onClick={add}>Add</button>
    </div>
  )
}

export default Counter