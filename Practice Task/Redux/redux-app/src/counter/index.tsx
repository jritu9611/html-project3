
import React from 'react'
import { useSelector,useDispatch} from 'react-redux'
import { action } from '../reducer/index'
function Counter() {
  const counter=useSelector((state:any)=>{
    return state.reducerFns.counter
})

  const dispatch=useDispatch()
  const increment=()=>{
    
    dispatch(action.increment(1))
    
  }
  const decrement=()=>{
    dispatch(action.decrement(-1))
  }
  const add=()=>{
    dispatch(action.add(10))
  }
   
   
  return (
    <div>
      <h1>Counter app</h1>
      <h2>{counter}</h2>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <button onClick={add}>Add</button>
    </div>
  )
}

export default Counter